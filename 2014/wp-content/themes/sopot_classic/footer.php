</div><!-- end #wrap -->

<!-- Footer -->
<div id="footer">
  <div class="footer-row fixed">
			
<?php
wz_setSection('zone-footer');
?>
    <div class="footer-col-left">
<?php
wz_setZone(220);
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-left'));
?>
    
    </div><!-- end .footer-col -->
				
    <div class="footer-col-loga">
<?php
wz_setZone(460);
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-center-left'));
?>
    
    </div><!-- end .footer-col -->
				
    <div class="footer-col-right">
<?php
wz_setZone(460);
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-center-right'));
?>
    
    </div><!-- end .footer-col -->
				
    
  </div><!-- end .footer-row fixed -->			
</div><!-- end #footer -->

<div class="footer-bottom"> 
  <div class="footer-row">
    <div class="footer-bottom-copyright">
<?php
if (get_option("bc_copyright") != '') {
    echo stripslashes(get_option("bc_copyright"));
} else {
?>
&copy;
<?php
    $the_year = date("Y");
    echo $the_year;
?>
 Sopot Classic

<?php
}
?>
    </div><!-- end .footer-bottom-copyright -->
    
    <div class="facebook">
    <a href="https://www.facebook.com/SopotClassic" target="_blank">
    	<img src="<?php echo get_template_directory_uri(); ?>/images/facebook.jpg" alt="facebook" />
    </a> 
    </div><!-- end .footer-bottom-social -->
    
    <div class="realizacja">
    <a href="http://quellio.com/" target="_blank" title="Quellio - We make your apps"><img src="<?php echo get_template_directory_uri(); ?>/images/quellio.png" alt="Quellio" /></a>
    </div>
    
    
  </div><!-- end .footer-row -->
</div><!-- end .footer-bottom -->


<?php  echo ' '. get_template_part( 'prettyPhotojs' ) . ''; ?>


<?php wp_footer(); ?>

</body>
</html>