<?php
/*
Template Name: Blog Style 1
*/
?>

<?php
get_header();
debug("1");
?>

 <div class="main-navigation">
				<?php 
                                       wp_nav_menu(array(
                                       'menu' => 'Main Menu', 
                                       'container_id' => 'clubbmenu', 
                                       'walker' => new CSS_Menu_Maker_Walker()
                                       )); 
                                       ?>	
     <ul id="lan">
         <li><?php  echo '<a href="'.get_bloginfo('url').'">PL</a> ' ?></li>
         <li><?php  echo '<a href="'.get_bloginfo('url').'/en/">/ EN</a>' ?></li>
     </ul>
      </div><!-- end .main-navigation -->			
    </div><!-- end #main -->
  </div><!-- end .header-row fixed -->           
</div><!-- end #header -->

<!-- Wrap -->
<div id="wrap">


<div id="content">

<div class="title-head"><h1>Aktualnośi-9ci</h1></div>

<?php
$page_layout = sidebar_layout();
switch ($page_layout) {
    case "layout-sidebar-left":
        echo '
<div class="sidebar-left">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-left -->';
        break;
    case "layout-sidebar-right":
        echo '
<div class="sidebar-right">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-right -->';
        break;
    case "layout-full":
        echo '
<div class="sidebar-right">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-right -->';
        break;
}
?>
	
<div class="fixed">  
  <div class="col-blog-archive">
<?php
$query    = array(
    'post_type' => 'post',
    'paged' => $paged
);
$wp_query = new WP_Query($query);
if (have_posts())
    debug("2");
    while ($wp_query->have_posts()):
        the_post();
debug("3");
        /*$image_id = get_post_thumbnail_id($post->ID);*/
        $cover    = wp_get_attachment_image_src($image_id, 'blog-preview');
        echo '
    <div class="blog-archive">';
        if ($image_id) {
        echo '
      <div class="blog-arc-cover">
        <a href="' . get_permalink() . '"><img src="' . $cover[0] . '" alt="' . get_the_title() . '" /></a>
      </div><!-- end .blog-arc-cover -->';
        }
        echo '
      <h2 class="blog-arc-heading"><a href="' . get_permalink() . '" rel="bookmark">' . get_the_title($post->ID) . '</a></h2>
      <div class="blog-arc-info">
        <p class="blog-date">' . get_the_time('F jS, Y') . '</p> 
        
      </div><!-- end .blog-arc-info -->';
        echo "<p>" . the_excerpt_max(350) . "</p>";
        echo '
      <div class="blog-arc-more"><a href="' . get_permalink() . '" rel="bookmark">Czytaj Dalej</a></div>
    </div><!-- end .blog-archive -->';
    endwhile;
?>

    <div class="pagination-pos">
            <?php
if (function_exists("pagination")) {
    pagination();
}
?>
    </div><!-- end .pagination-pos -->								
  </div><!-- end .col-blog-archive -->   			 
</div><!-- end .fixed -->
</div><!-- end #content -->

<?php
get_footer();
?>