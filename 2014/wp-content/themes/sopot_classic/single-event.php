<?php
get_header();



if($GLOBALS['current_language'] == 'e') {
        $names = array('Location:', 'Time:', 'Conductor:', 'Soloists:', 'Orchestra:');
} else {  
    $names = array('Miejsce:', 'Godzina:' , 'Dyrygent:', 'Soliści:', 'Orkiestra:');
}
?>

 <div class="main-navigation">
				<?php 
                                       wp_nav_menu(array(
                                       'menu' => 'Main Menu', 
                                       'container_id' => 'clubbmenu', 
                                       'walker' => new CSS_Menu_Maker_Walker()
                                       )); 
                                       ?>	
     <ul id="lan">
         <li><?php  echo '<a href="'.get_bloginfo('url').'">PL</a> ' ?></li>
         <li><?php  echo '<a href="'.get_bloginfo('url').'/en/">/ EN</a>' ?></li>
     </ul>
      </div><!-- end .main-navigation -->			
    </div><!-- end #main -->
  </div><!-- end .header-row fixed -->           
</div><!-- end #header -->

<!-- Wrap -->
<div id="wrap">

<div id="content">

<div class="title-head"><h1><?php
echo $GLOBALS['current_language'] == 'e' ? 'EVENT' : 'WYDARZENIE';
?>
</h1></div>


<?php
$page_layout = of_get_option('event_images');
switch ($page_layout) {
    case "left-event-sidebar":
        echo '
<div class="sidebar-left">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-left -->';
        break;
    case "right-event-sidebar":
        echo '
<div class="sidebar-right">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-right -->';
        break;
}
?>

<div class="fixed">
  <div class="single-col">					
  
<?php
if (have_posts())
    while (have_posts()):
        the_post();
        global $post;
        $results        = $wp_query->post_count;
        $data_event     = get_post_meta($post->ID, 'event_date_interval', true);
        $time           = strtotime($data_event);
        $pretty_date_yy = date('Y', $time);
        $pretty_date_d  = date('d', $time);
		$pretty_date_M2  = iconv("ISO-8859-2","UTF-8", ucfirst(strftime('%B', $time)));
debug(date('YYYY MM dd', $time));
		require('includes/language.php');
        $image_id       = get_post_thumbnail_id();
        $cover          = wp_get_attachment_image_src($image_id, 'event-cover-arc');
        $image_id       = get_post_thumbnail_id();
        $event_location = get_post_meta($post->ID, "event_location", true);
        $event_venue    = get_post_meta($post->ID, "event_venue", true);
        $tstart         = get_post_meta($post->ID, 'event_tstart', true);
    $tstart_parsed = parseTime($tstart, $GLOBALS['current_language']);
        $tend           = get_post_meta($post->ID, 'event_tend', true);
    $tend_parsed = parseTime($tend, $GLOBALS['current_language']);
        $event_ticket   = get_post_meta($post->ID, "event_ticket", true);
		$event_text     = get_post_meta($post->ID, "ev_text", true);
        $event_out      = get_post_meta($post->ID, "event_out", true);
        $event_cancel   = get_post_meta($post->ID, "event_cancel", true);
        $event_zoom     = get_post_meta($post->ID, "event_zoom", true);
        $coordinated    = get_post_meta($post->ID, "event_coordinated", true);
        $club           = get_post_meta($post->ID, "event_venue", true);
        $event_allday   = get_post_meta($post->ID, "event_allday", true, true);

        $event_conductor= get_post_meta($post->ID, 'event_conductor', true);    
        $event_conductor_link= get_post_meta($post->ID, 'event_conductor_link', true);    
        $event_soloists  = get_post_meta($post->ID, 'event_soloists', true);    
        $event_soloists_links  = get_post_meta($post->ID, 'event_soloists_links', true);    
        //$event_orchestra= _e(get_post_meta($post->ID, 'event_orchestra', true));  

$event_soloists_arr = explode("\n", $event_soloists);
if(count($event_soloists_arr) == 1)
    $event_soloists_arr = explode(",", $event_soloists);

$event_soloists_links_arr = explode("\n", $event_soloists_links);
if(count($event_soloists_links_arr) == 1)
    $event_soloists_links_arr = explode(",", $event_soloists_links);

$j = 0;
$solo_arr_size = count($event_soloists_arr);
$link_arr_size = count($event_soloists_links_arr);

debug($event_soloists_arr);
debug($event_soloists_links_arr);
        echo '
    <div class="event-cover">';
        if ($image_id) {
            echo '
      <img src="' . $cover[0] . '" alt="' . get_the_title() . '" />';
        } else {
            echo '
      <img src="' . get_template_directory_uri() . '/images/no-featured/event-single.png" alt="no image" />';
        }
        if ($data_event != null) {
            echo '        
      <div class="event-single-data">
        <div class="event-single-day">' . $pretty_date_d . '</div>
        <div class="event-single-month">' . ($GLOBALS['current_language'] == 'e' ? $pretty_date_M2 : date_declension($pretty_date_M2)) . '</div>
        <div class="event-single-year">' . $pretty_date_yy . '</div>
      </div>            
    </div><!-- end .event-cover -->';
        }
        echo '
    <div class="event-text">
      <h2 class="event-title">' . get_the_title($post->ID) . '</h2>
        <ul class="event-meta">';
        if ($event_location != null) {
            echo '<li><span style="vertical-align:top">' . $names[0] . '</span>';
            _e($event_location);
            echo '</li>';
        }           
        echo '<li><span>' . $names[1] . '</span>' . $tstart_parsed . '';              
        echo '<li><span style="vertical-align:top">' . $names[2] . '</span><span style="width:auto;color:#000"><a href="../../' . $event_conductor_link . '">' . $event_conductor . '</a></span></li>';    
        echo '<li><span style="vertical-align:top">' . $names[3] . '</span><span style="width:auto;color:#000">';

        for($i = 0; $i < $solo_arr_size; $i++) {
            $href = $j < $link_arr_size ? '../../' . $event_soloists_links_arr[$j++] : '../../artysci/solisci/';
            echo '<a href="' . $href . '">';
            _e($event_soloists_arr[$i]);
            echo '</a><br>';
        }

        echo '</span></li>';    
        echo '<li><span>' . $names[4] . '</span>';
        _e(get_post_meta($post->ID, 'event_orchestra', true));
        echo '</li>';    
        echo '
          <li>';
	if (get_post_meta($post->ID, 'event_disable', true) == 'no') {
        if ($event_text) {   
            echo '<div class="event-tickets3" style="display:inline-block"><a class="bilety_link" style="font-size:12px;margin-top:9px;margin-left:0px;"" href="' . $event_ticket . '" target="_blank">' . ($GLOBALS['current_language'] == 'e' ? 'BUY TICKET ONLINE' : 'KUP BILET ONLINE') . '</a></div>';
            echo '<div class="blog-arc-more" style="display:inline-block"><a class="bilety_link" style="margin:9px 0 2px 10px;padding:5px 8px;" href="' . ($GLOBALS['current_language'] == 'e' ? 'http://sopotclassic.quellio.pl/en/bilety/">INFO ABOUT TICKETS' : 'http://sopotclassic.quellio.pl/bilety/">INFO O BILETACH') . '</a></div>';
        } else {  
            if (get_post_meta($post->ID, 'event_out', true) == 'yes') {
                echo '
                        <div class="event-cancel-out"><p>Sold Out</p></div>';
            } elseif (get_post_meta($post->ID, 'event_cancel', true) == 'yes') {
                echo '
                        <div class="event-cancel-out"><p>Canceled</p></div>';
            } elseif (get_post_meta($post->ID, 'event_free', true) == 'yes') {
                echo '
                        <div class="event-cancel-out"><p>Free Entry</p></div>';
            } else {
                echo '
                        <div class="event-tickets"><a href="' . $event_ticket . '" target="_blank">Buy Tickets</a></div>';
            }
        }
	}
        echo '</li>
        </ul><!-- end ul.event-meta -->';
        echo '
            ' . the_content() . '                                
    </div><!-- end .event-text -->';
        if ($coordinated != null) {
            echo '
       <script type="text/javascript">
      jQuery(document).ready(function($){
      
        $("#event-map").gmap3({
          marker:{
            latLng: [' . $coordinated . '],
            options:{
              draggable:true
            },
            events:{
              dragend: function(marker){
                $(this).gmap3({
                  getaddress:{
                    latLng:marker.getPosition(),
                    callback:function(results){
                      var map = $(this).gmap3("get"),
                        infowindow = $(this).gmap3({get:"infowindow"}),
                        content = results && results[1] ? results && results[1].formatted_address : "no address";
                      if (infowindow){
                        infowindow.open(map, marker);
                        infowindow.setContent(content);
                      } else {
                        $(this).gmap3({
                          infowindow:{
                            anchor:marker, 
                            options:{content: content}
                          }
                        });
                      }
                    }
                  }
                });
              }
            }
          },
          map:{
            options:{
              zoom: ' . $event_zoom . '
            }
          }
        });
        
      });
    </script>
 
    <div id="event-map"></div>';
        }
        echo '       
  </div><!-- end .single-col -->';
    endwhile;
?>

</div><!-- end .fixed -->
</div><!-- end #content -->
	
<?php
get_footer();
?>