<?php
get_header();
?>

 <div class="main-navigation">
				<?php 
                                       wp_nav_menu(array(
                                       'menu' => 'Main Menu', 
                                       'container_id' => 'clubbmenu', 
                                       'walker' => new CSS_Menu_Maker_Walker()
                                       )); 
                                       ?>	
     <ul id="lan">
         <li><?php  echo '<a href="'.get_bloginfo('url').'">PL</a> ' ?></li>
         <li><?php  echo '<a href="'.get_bloginfo('url').'/en/">/ EN</a>' ?></li>
     </ul>
      </div><!-- end .main-navigation -->			
    </div><!-- end #main -->
  </div><!-- end .header-row fixed -->           
</div><!-- end #header -->

<!-- Wrap -->
<div id="wrap">

<div id="content">

<div class="title-head"><h1><?php
the_title();
?>
</h1></div>
<?php
generate_thumbnail_list(get_the_ID());
?>  

</div><!-- end #content -->

	
<?php
get_footer();
?>
