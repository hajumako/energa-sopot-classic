<?php get_header(); ?>

 <div class="main-navigation">
				<?php 
                                       wp_nav_menu(array(
                                       'menu' => 'Main Menu', 
                                       'container_id' => 'clubbmenu', 
                                       'walker' => new CSS_Menu_Maker_Walker()
                                       )); 
                                       ?>	

      </div><!-- end .main-navigation -->			
    </div><!-- end #main -->
  </div><!-- end .header-row fixed -->           
</div><!-- end #header -->

<!-- Wrap -->
<div id="wrap">

<div id="content">

  <div class="title-head"><h1>404 ERROR - Not Found</h1></div>
  <div class="content-404">
    <h4>The page you requested does not exist.</h4>
  </div><!-- end .content-404 -->

</div><!-- end #content -->

	
<?php get_footer(); ?>