<?php
get_header();
?>

 <div class="main-navigation">
				<?php 
                                       wp_nav_menu(array(
                                       'menu' => 'Main Menu', 
                                       'container_id' => 'clubbmenu', 
                                       'walker' => new CSS_Menu_Maker_Walker()
                                       )); 
                                       ?>	
     <ul id="lan">
         <li><?php  echo '<a href="'.get_bloginfo('url').'">PL</a> ' ?></li>
         <li><?php  echo '<a href="'.get_bloginfo('url').'/en/">/ EN</a>' ?></li>
     </ul>
      </div><!-- end .main-navigation -->			
    </div><!-- end #main -->
  </div><!-- end .header-row fixed -->           
</div><!-- end #header -->

<!-- Wrap -->
<div id="wrap">


<div id="content">

<div class="title-head"><h1><?php
$category = get_the_category();
echo $category[0]->cat_name;
?>
</h1></div>

<?php
$page_layout = of_get_option('blog_images');
switch ($page_layout) {
    case "left-blog-sidebar":
        echo '
<div class="sidebar-left">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-left -->';
        break;
    case "right-blog-sidebar":
        echo '
<div class="sidebar-right">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-right aa-->';
        break;
}
?>

<div class="fixed">
  <div class="single-col">					
<?php
if (have_posts())
    while (have_posts()):
        the_post();
        $image_id = get_post_thumbnail_id($post->ID);
        $cover    = wp_get_attachment_image_src($image_id, 'blog-preview');
for($k = 0; $k < count(cover); $k++) {
 debug($cover[$k]);   
}
        $pretty_date_M2  = iconv("ISO-8859-2","UTF-8", get_the_time('j F Y'));
        if ($image_id) {
            echo '
    <div class="blog-arc-cover">     
        <img src="' . $cover[0] . '" alt="' . get_the_title() . '" />
    </div><!-- end .blog-arc-cover -->';
        }
        echo '  
    <h2 class="blog-arc-heading">' . get_the_title($post->ID) . '</h2>
      <div class="blog-arc-info">
        
        <p class="blog-date">' . ($GLOBALS['current_language'] == 'e' ? get_the_time('F jS, Y') : date_declension($pretty_date_M2)) . '</p> 
        
      </div><!-- end .blog-arc-info -->';
        echo "<p>" . the_content() . "</p>";
    endwhile;
?>			
				
<?php
comments_template('', true);
?>

    </div><!-- end .single-col -->			
</div><!-- end .fixed -->		 
</div><!-- end #content -->
	
<?php
get_footer();
?>