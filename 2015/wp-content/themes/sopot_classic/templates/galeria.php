<?php

/*
Template Name: .OBECNA Galeria
*/

?>

<?php
get_header();
?>

<style type="text/css" rel="stylesheet">  
	.col-post-media {
		float:none;
	}
</style> 

<div class="title-head">
	<h1><?php the_title(); ?></h1>
</div>

<div class="col-post-media">
	<h3 id="video"><?php echo pll_current_language() == 'en' ? 'Video' : 'Wideo'; ?></h3>
	<div class="post-media">
	<?php
		$query = array(
			'post_type' => 'video',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order'   => 'DESC',
			'taxonomy' => 'videos',
			'term' => null
		); 
		$wp_query  = new WP_Query($query);
		while ($wp_query->have_posts()):
			$wp_query->the_post();
			global $post;
			$results = $wp_query->post_count;
			$video    = get_post_meta($post->ID, "video_link", true);
			$image_id = get_post_thumbnail_id();
			$cover    = wp_get_attachment_image_src($image_id, 'video-archive');
			
			?>                                    
			<div class="media-arc last-p">
				<div class="video-arc-cover bar-arc-video">
					<a href="<?php echo $video; ?>" data-rel="prettyPhoto">
					<?php
						if ($image_id) {
							echo '<img src="' . $cover[0] . '" alt="' . get_the_title() . '" />';
						} else {
							echo '<img src="' . get_template_directory_uri() . '/images/no-featured/photo-video.png" alt="no image" />';
						}?>
						<div class="media-title mosaic-overlay">
						<?php 
						if (strlen($post->post_title) > 32) {
							echo mb_substr(the_title($before = '', $after = '', FALSE), 0, 30,'UTF-8') . '...';
						} else {
							the_title();
						}
						?>				
						</div>
					</a>
				</div><!-- end .photo-arc-cover -->
			</div><!-- end .media-arc last-p -->
	<?php 
		endwhile; 
	?>
	</div><!-- end .post-media -->
</div><!-- end .col-post-media -->


    <div id="photos">
<?php
	$editions = get_terms('edycjanr', 'orderby=name&order=DESC&hide_empty'); 

	foreach($editions as $edition) :
		 
                $query = array(
					'post_type' => 'photo',
					'posts_per_page' => -1,
					'edycjanr' => $edition->slug
				); 
				$wp_query  = new WP_Query($query);
        if ( $wp_query->have_posts() ) : ?>
		<div class="col-post-media">
			<h3 id="<?php echo $edition->slug ?>"><?php pll_e($edition->name) ?></h3>
			<div class="post-media">
			<?php
				
				while ($wp_query->have_posts()):
					$wp_query->the_post();
					global $post;
					$results = $wp_query->post_count;
					$image_id = get_post_thumbnail_id();
					$cover = wp_get_attachment_image_src($image_id, 'photo-archive');

					$permalink = get_permalink();
					
					?>                                    
					<div class="media-arc last-p">
						<div class="photo-arc-cover bar-arc-photo">
							<a href="<?php echo get_permalink() ?>">
							<?php
								if ($image_id) {
									echo '<img src="' . $cover[0] . '" alt="' . get_the_title() . '" />';
								} else {
									echo '<img src="' . get_template_directory_uri() . '/images/no-featured/photo-video.png" alt="no image" />';
								}?>
								<div class="media-title mosaic-overlay">
								<?php 
								if (strlen($post->post_title) > 32) {
									echo mb_substr(the_title($before = '', $after = '', FALSE), 0, 30,'UTF-8') . '...';
								} else {
									the_title();
								}
								?>				
								</div>
							</a>
						</div><!-- end .photo-arc-cover -->
					</div><!-- end .media-arc last-p -->
			<?php 
				endwhile; 
			?>
			</div><!-- end .post-media -->
		</div><!-- end .col-post-media -->
    <?php
        endif;


	endforeach;

?></div>
<?php
get_footer();
?>
