<?php

/*
Template Name: .POPRZEDNIE Artyści 
*/

?>

<?php
get_header();
?>

<style rel="stylesheet" type="text/css">
	.artist-content {
		max-height:172px; 
		overflow:hidden;
		height:auto;
		margin-bottom: 15px !important;
		transition: all ease-in-out .7s;
		-o-transition: all ease-in-out .7s;
		-moz-transition: all ease-in-out .7s;
		-webkit-transition: all ease-in-out .7s;
	}
	.blog-home-cover {
		height:175px;
	}
	h2.event-arc-title {
		margin-bottom: 0;
		line-height:1em;
	}
	.artist-content p {	
		margin-bottom: 13px;
	}
	.more-wrapper {  
		float: right;
		margin: 9px 20px 9px 0;
		background: #fff;
		text-transform: uppercase;
		text-decoration: none;
		border: 1px solid #aaaaaa;
		color: #888888;
		padding: 6px 10px;
		font-size: 12px;
		cursor: pointer;
		width: 100px;
		height: 20px;
		position: relative;
		overflow:hidden;
		transition: all ease-in-out .7s;
		-o-transition: all ease-in-out .7s;
		-moz-transition: all ease-in-out .7s;
		-webkit-transition: all ease-in-out .7s;
	}
	.translated div {
		transform: translate(0,-30px);
		-o-transform: translate(0,-30px);
		-ms-transform: translate(0,-30px);
		-moze-transform: translate(0,-30px);
		-webkit-transform: translate(0,-30px);
	}
	.more-wrapper div {
		height: 30px;
		left:0;
		right:0;
		margin:0;
		text-align:center;
		transition: all ease-in-out .3s;
		-o-transition: all ease-in-out .3s;
		-moz-transition: all ease-in-out .3s;
		-webkit-transition: all ease-in-out .3s;
	}
	.more-wrapper:hover {
		background: #cecece;
		border-color: #cecece;
	}
	.more-wrapper:hover div {
		color: #ffffff;
	}
</style>

<?php
	global $post;
	$location = str_replace(array(strtolower(get_bloginfo('url'))), '', strtolower(get_permalink()));
	$page_layout = sidebar_layout();
	$slide_nr = of_get_option('nr_slide');
	$slide_seconds = of_get_option('seconds_slide');
	$post_types = array(
		array(
			'name' => 'dyrygent',
			'header' => pll__('dyrygenci')
		),
		array(
			'name' => 'solista',
			'header' => pll__('solisci')
		), 
		array(
			'name' => 'prowadzacy-1',
			'header' => pll__('prowadzacy')
		)
	);
	if (strlen($location) > 2) : 
?>
		<div class="title-head">
			<h1><?php echo get_the_title(); ?></h1>
		</div>
		<div class="fixed">
			<div class="content-right">
			<?php 
				foreach($post_types as $post_type) : ?>
					<div class="content-right-section" style="padding-top: 20px">
						<div class="title-home artysci" style="margin-top:0px;">
							<h3><?php echo $post_type['header']; ?></h3>
						</div>
						<div class="home-post">
						<?php 
							$custom_query = new WP_Query('post_type=' . $post_type['name'] . '&edycjanr=' . get_edition_number($post->ID) . '&posts_per_page=-1');
							while($custom_query->have_posts()) : 
								$custom_query->the_post(); 
								global $post;
								$image_id    = get_post_thumbnail_id();
								$cover_blog  = wp_get_attachment_image_src($image_id, 'blog-home'); 
                                $cover  = wp_get_attachment_image_src($image_id, 'Original' );  
								$pretty_date_M2  = iconv("ISO-8859-2","UTF-8", get_the_time('j F Y'));
						?>
							<div id="<?php echo $post->post_name ?>" class="home-width fixed accordion">
								<div class="blog-home artist-content">	
									<div class="content-wrapper">
									<?php if ($image_id) : ?>
										<div class="blog-home-cover">
											<a href="<?php echo $cover[0]; ?>" class="fancybox" rel="group">
												<img src="<?php echo $cover_blog[0]; ?>" alt="<?php echo get_the_title(); ?>" />
											</a>
										</div>
									<?php endif; ?>
										<h2 class="event-arc-title">
											<?php echo get_the_title(); ?>
										</h2>
										<p><?php the_content(); ?></p>
									</div>
								</div> 
								<div class="more-wrapper">
									<div><?php pll_e('more'); ?></div>
									<div><?php pll_e('less'); ?></div>
								</div>			
							</div>
						<?php 
							endwhile;
							wp_reset_query(); ?>
						</div>
					</div>   
				<?php endforeach;?>
			</div>
   
			<div class="sidebar-right">
			<?php 
			wz_setSection('zone-sidebar');
			if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
			?>
			</div>          
		</div>
		
<script type="text/javascript">
	var articles,
		style,
		min_h = 170;

	jQuery(document).ready(function () {
		articles = jQuery('.accordion');
		adjustContentPosition();
		jQuery('.more-wrapper').click(function () {
			jQuery('.artist-content', jQuery(this).parent()).toggleClass('expanded');
			jQuery(this).toggleClass('translated');
		});
		if(window.location.hash.length) {
			jQuery(window.location.hash + ' .artist-content').addClass('expanded');	
			jQuery(window.location.hash + ' .more-wrapper').addClass('translated');
		}	
	});

	jQuery(window).resize(adjustContentPosition);
	jQuery(window).load(adjustContentPosition);

	function adjustContentPosition() {
		if (style !== undefined)
			style.remove();
		var styleContent = '';
		articles.each(function () {
			h = jQuery('.artist-content .content-wrapper', this).height();
			if (h <= min_h) {
				jQuery('.more-wrapper', this).hide();
			} else {
				jQuery('.more-wrapper', this).show();
			}
			styleContent += "#" + jQuery(this).attr('id') + " .artist-content.expanded {max-height:" + (h > min_h ? h + "px" : 'none') + ";}"
		});
		style = jQuery("<style />", {
			id: 'articleHeightStyle',
			type: 'text/css',
			html: styleContent
		});
		style.appendTo('head');
	}
</script>

<?php 
	endif; 
	get_footer();
?>