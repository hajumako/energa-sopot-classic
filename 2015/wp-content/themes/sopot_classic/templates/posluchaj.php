<?php

/*
Template Name: POSLUCHAJ PAGE
*/

?>

<?php
get_header();
?>

<style type="text/css" rel="stylesheet">  
	.col-post-media {
		float:none;
        background-color: transparent !important;
        text-align: center;
	}
    .title-head h1 {
        color: #fff !important;
    }
    .title-head {
        text-align: center;
        margin: 2em 0;
	}
    
    .posluchaj-txt {
        font-size: 1.5em;
        color: #fff;
        margin: 60px auto;
        line-height: 1em;
    }
    
    a#posluchaj-btn {
		font-family: 'candararegular';
		background: orange;
		color: #fff;
		font-size: 1.7em;
		display: inline-block;
		margin: 0px auto 30px;
		padding: 0.5em 1em;      
    }
    
    a#posluchaj-btn:hover {
        background-color: white !important;
        color: orange !important;
    }
	
	.video-widget-cover {
		height: 169px;
		width: 300px;
		display: block;
		margin: 30px auto;
		float: none;
	}
	
	.video-widget-cover img {
		width: 100%;
	}
	
	.bar-widget-video .mosaic-overlay {
		background-position: center;
		padding: 11px 0;
		height: 169px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
	}

</style> 

<div class="title-head">
	<h1>Posłuchaj utworu w wykonaniu Gershwina i najpiękniejszych coverów</h1>
</div>

<div class="col-post-media">

	<div class="video-widget-cover bar-widget-video">
		<a href="https://www.youtube.com/watch?v=xqGI8sSgXbE&index=1&list=PLt0nSwkpc_jNFyjonnMMBhgasrAPUNrP-" target="_blank">
			<img src="<? echo get_template_directory_uri() ?>/images/Swanee.jpg" alt="Swanne"/>
			<div class="media-title mosaic-overlay">
				Swanee
			</div>
		</a>
	</div>	
	<div class="video-widget-cover bar-widget-video">
		<a href="https://www.youtube.com/watch?v=xJOtaWyEzaI&list=PLt0nSwkpc_jNFyjonnMMBhgasrAPUNrP-&index=2" target="_blank">
			<img src="<? echo get_template_directory_uri() ?>/images/Summertime.jpg" alt="Summertime"/>
			<div class="media-title mosaic-overlay">
				Summertime
			</div>
		</a>
	</div>
	<div class="video-widget-cover bar-widget-video">
		<a href="http://www.dwateatry.tvp.pl/wideo/leszek-mozdzer--blekitna-rapsodia-georgea-gershwina-20640357/" target="_blank">
			<img src="<? echo get_template_directory_uri() ?>/images/RhapsodyInBlue.jpg" alt="RhapsodyInBlue"/>
			<div class="media-title mosaic-overlay">
				Rhapsody in Blue
			</div>
		</a>
	</div>
	
	<div id="bottom-part">
		<div class="posluchaj-txt">
			<p>Chcesz posłuchać muzyki Gershwina na żywo?</p>
			<p>Kup bilet na Koncert inauguracyjny i przyjdź na Festiwal!</p>
		</div>
		
		<div>
			<a href="http://sopotclassic.pl/bilety/" rel="bookmark" id="posluchaj-btn">BILETY</a>
		</div>
	
	</div>
</div><!-- end .col-post-media -->



<?php
get_footer();
?>
