<?php 

/*Template Name: .EDYCJA*/
 
?>

<?php
get_header();
?>

<?php
global $post;
$location      = str_replace(array(
    strtolower(get_bloginfo('url'))
), '', strtolower(get_permalink()));

$page_layout   = sidebar_layout();
$slide_nr      = of_get_option('nr_slide');
$slide_seconds = of_get_option('seconds_slide');
if (strlen($location) > 4) {
//    echo '
// <div class="title-head"><h1>' . get_the_title() . '</h1></div>';
    
    //GET THE NUMBER OF EDITION
        /*$terms = get_the_terms( $post->ID, 'edycjanr' );

        if ( $terms && ! is_wp_error( $terms ) ) : 

            $edycjanr = array();

            foreach ( $terms as $term ) {
                $edycjanr[] = $term->name;
            }

            $nazwaedycji = join( ", ", $edycjanr );

         endif; 

        $edition_number = preg_replace("/[^0-9]/","",$nazwaedycji); */
    
     
    //if (of_get_option('active_slide', '1') == '1') {
        $wp_slider_query = new WP_Query(array(
            'post_type' => 'slide',
            'posts_per_page' => -1,
            'orderby' => 'DATE',
            'order' => 'DESC' ,
            'edycjanr' => get_edition_number($post->ID)
        ));
        echo '
<div id="slide"> 
  <div class="cycle-slideshow" data-cycle-timeout="' . $slide_seconds . '" data-cycle-next="#next1" data-cycle-prev="#prev1" data-cycle-slides="div.slide">';
    if ($wp_slider_query->post_count >= 2) {
        echo '<a id="prev1" href="#"><div class="cycle-prev"></div></a>
        <a id="next1" href="#"><div class="cycle-next"></div></a>';
    }
        if ($wp_slider_query->post_count) {
            while ($wp_slider_query->have_posts()):
                $wp_slider_query->the_post();
                $custom    = get_post_custom($post->ID);
                $image_id  = get_post_thumbnail_id();
                $cover     = wp_get_attachment_image_src($image_id, 'slider-full');
                $slide_des = $custom["slide_des"][0];
                $slide_url = $custom["slide_url"][0];
                $slide_title = get_the_title();
                echo '
    <div class="slide">';
                if ($slide_url != null) {
                    echo '
      <a href="' . $slide_url . '">';
                }
                echo '
        <img src="' . $cover[0] . '" alt="' . get_the_title() . '" />';
                if ($slide_url != null) {
                    echo '
      </a>';
                }
                if ($slide_url != null) {
                    echo '
      <a href="' . $slide_url . '">';
                }
                
                if ($slide_title != null) {
                echo '
        <div class="slide-title">' . $slide_title . '</div>';
                }
                
                if ($slide_url != null) {
                    echo '
      </a>';
                }
                if ($slide_url != null) {
                    echo '
      <a href="' . $slide_url . '">';
                }
                if ($slide_des != null) {
                    echo '
        <div class="slide-desc">' . $slide_des . '</div>';
                }
                if ($slide_url != null) {
                    echo '
      </a>';
                }
                echo '
    </div><!-- end .slide -->';
            endwhile;
            echo '
  </div><!-- end .cycle-slideshow -->
</div><!-- end #slide -->';
        }
    //}
    
    switch ($page_layout) {
        case "layout-sidebar-left":
            echo '<div class="fixed">';
            echo '
<div class="content-left">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .content-left -->';
            echo '
<div class="sidebar-left">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-left -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-sidebar-right":
            echo '<div class="fixed">';
            echo '
<div class="content-home-right">';
        
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
        
        echo '<div class="title-home"><h3>'; 
        pll_e('aktualnosci'); 
        echo '</h3></div>';
        
         ?>
        <?php 
        
        //GET THE NUMBER OF EDITION
        /*$terms = get_the_terms( $post->ID, 'edycjanr' );

        if ( $terms && ! is_wp_error( $terms ) ) : 

            $edycjanr = array();

            foreach ( $terms as $term ) {
                $edycjanr[] = $term->name;
                $id_of_term = $term->term_id;
            }

            $nazwaedycji = join( ", ", $edycjanr );
        
            $wyglad =  json_decode(get_option('wyglad-'.$id_of_term), true);
        
        ?>
        
        
        <?php endif; 

        $edition_number = preg_replace("/[^0-9]/","",$nazwaedycji); */
        
        $custom_query = new WP_Query('post_type=post&edycjanr=' . get_edition_number($post->ID) .'');
        echo '<div class="home-post">';
while($custom_query->have_posts()) : $custom_query->the_post(); 

        global $post;
        $image_id    = get_post_thumbnail_id();
        $cover_blog  = wp_get_attachment_image_src($image_id, 'blog-home');    
        $pretty_date_M2  = iconv("ISO-8859-2","UTF-8", get_the_time('j F Y'));
        debug('x'. $GLOBALS['current_language']);


?>
        <div class="home-width fixed">
        <div class="blog-home">
            
      <?php if ($image_id) : 
         echo    '<div class="blog-home-cover">
          <a href="' . get_permalink() . '">
            <img src="' . $cover_blog[0] . '" alt="' . get_the_title() . '" />
          </a>
        </div><!-- end .blog-home-cover --> ';
       endif; 
        echo
        '<h2 class="event-arc-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>
        <div class="blog-home-info">
          
          <p class="blog-date">' . (pll_current_language() == 'en' ? get_the_time('F jS, Y') : date_declension($pretty_date_M2)) . '</p> 
          
        </div><!-- end .blog-home-info -->
        <p>' . the_excerpt_max(200) . '</p>
        <div class="blog-arc-more"><a href="' . get_permalink() . '" rel="bookmark">';
        pll_e('more'); 
        echo '</a></div>
      </div><!-- end .blog-home -->                   
    </div><!-- end .home-width fixed -->';
    endwhile;
    wp_reset_query();
       echo '</div><!-- end .home-post -->'; 
        
     echo '<div class="title-home"><h3>'; 
        pll_e('zdjecia'); 
        echo '</h3></div>';
        
       
        
//    PHOTOS   
     $query1 = array(
            'post_type' => 'photo',
            'orderby' => $orderby,
            'order' => $order,
            'posts_per_page' => $items,
            'edycjanr' => get_edition_number($post->ID)
        );   
        
    $wp_query_photo = new WP_Query($query1);
   
  echo '<div class="home-post fixed">
    <div class="col-home">
      <div class="home-width">';
    while ($wp_query_photo->have_posts()):
        $wp_query_photo->the_post();
        global $post;
        $fix         = the_excerpt_max(0);
        $title       = get_the_title($fix);
        $image_id    = get_post_thumbnail_id();
        $cover_photo = wp_get_attachment_image_src($image_id, 'photo-home');
        echo '
        <div class="photo-home last-p">
          <div class="photo-home-cover bar-home-photo">
            <a href="' . get_permalink() . '">';
        if ($image_id) {
            echo '
              <img src="' . $cover_photo[0] . '" alt="' . get_the_title() . '" />';
        } else {
            echo '
              <img src="' . get_template_directory_uri() . '/images/no-featured/photo-video-home.png" alt="no image" />';
        }
        echo '
              <div class="media-home-title mosaic-overlay">';             
        if (strlen($post->post_title) > 29) {
        echo ' ' . substr(the_title($before = '', $after = '', FALSE), 0, 29).  '...';    ' ';
        } else {
        echo ' ' . $title . ' ';
        }      
        echo '</div><!-- end .audio-title -->    
            </a>
          </div><!-- end .photo-home-cover -->          
        </div><!-- end .photo-home last-p -->';
    endwhile;
    wp_reset_query();
        echo '</div><!-- end .home-width -->
    </div><!-- end .col-home -->
  </div><!-- end .home-post fixed-->';
        
            
            
 
   
        
        
        
            echo '
</div><!-- end .content-right -->';
            echo '
<div class="sidebar-right">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-right -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-full":
            echo '
<div class="single-page-col">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .single-page-col -->';
            break;
        
        
        
        
        
        
        
    }
} else { 
    
    switch ($page_layout) {
        case "layout-sidebar-left":
            echo '
<div class="fixed">';
            echo '
<div class="content-home-left">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .content-home-left -->';
            echo '
<div class="sidebar-left">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-left -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-sidebar-right":
            echo '
<div class="fixed">';
            echo '
<div class="content-home-right">';
        
        
            echo '
</div><!-- end .content-home-right -->';
            echo '
<div class="sidebar-right">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-right -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-full":
            echo '
<div class="single-page-col">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .single-page-col -->';
            break;
    }
}
?>


<?php
get_footer();
?>