<?php

/*
Template Name: .POPRZEDNIA Galeria
*/

?>

<?php
get_header();
?>

<div id="content">

<div class="title-head"><h1><?php
the_title();
?>
</h1></div>
<?php

   $query = array(
		'post_type' => 'photo',
		'orderby' => $orderby,
		'order' => $order,
		'posts_per_page' => -1,
		'edycjanr' => get_edition_number($post->ID)
	);   
	
	$galleries_items_nr = array();
	$galleries_items_title = array();


	$wp_query_photo = new WP_Query($query);
	while ($wp_query_photo->have_posts()):
		$wp_query_photo->the_post();
		$galleryID = $post->ID;
		$nazwa = get_the_title();
		?> <h3 class="gallery-title"> <?php
		the_title();
		?> </h3> <?php
		$cnt_images = generate_thumbnail_list($galleryID);

		array_push($galleries_items_nr, $cnt_images);
		array_push($galleries_items_title, $nazwa);
	endwhile;
   
?>

</div><!-- end #content -->

<script type="text/javascript">
    var galleries_items_nr = <?php echo json_encode($galleries_items_nr); ?>;
    var galleries_items_title = <?php echo json_encode($galleries_items_title); ?>;
	
	function get_gallery_title(position) {
		cnt = 0;
		sum = galleries_items_nr[0];
		while(position > sum) {
			sum += galleries_items_nr[++cnt];
		}
		return galleries_items_title[cnt];
	}
</script>

<?php
get_footer();
?>
