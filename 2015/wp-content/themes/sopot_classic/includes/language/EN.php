<?php

	$months = array(
      'Styczeń',
      'Luty',
      'Marzec',
      'Kwiecień',
      'Maj',
      'Czerwiec',
      'Lipiec',
      'Sierpień',
      'Wrzesień',
      'Listopad',
      'Październik',
      'Grudzień'
    );
    $weekday = array(
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday'
    );
    $id_months = intval(strftime("%m", strtotime($data_event))) - 1;
	$id_weekday = ( 7 + intval(strftime("%w", strtotime($data_event))) - 1) % 7;
    $trans_months = htmlentities(utf8_decode($months[$id_months]));
	$trans_weekday = htmlentities(utf8_decode($weekday[$id_weekday]));
    $pretty_date_M = $trans_months;
	$pretty_date_w = $trans_weekday;
	
?>