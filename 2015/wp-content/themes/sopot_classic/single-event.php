<?php
get_header();

?>

<div id="content">

<div class="title-head"><h1><?php
 pll_e('wydarzenie');
?>
</h1></div>


<?php
$page_layout = of_get_option('event_images');
switch ($page_layout) {
    case "left-event-sidebar":
        echo '
<div class="sidebar-left">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-left -->';
        break;
    case "right-event-sidebar":
        echo '
<div class="sidebar-right">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-right -->';
        break;
}
?>

<div class="fixed">
  <div class="single-col">					
  
<?php
if (have_posts())
    while (have_posts()):
        the_post();
        global $post;
        $results        = $wp_query->post_count;
        $data_event     = get_post_meta($post->ID, 'event_date_interval', true);
        $time           = strtotime($data_event);
        $pretty_date_yy = date('Y', $time);
        $pretty_date_d  = date('d', $time);
		$pretty_date_M2  = iconv("ISO-8859-2","UTF-8", ucfirst(strftime('%B', $time)));
		require('includes/language.php');
        $image_id       = get_post_thumbnail_id();
        $cover          = wp_get_attachment_image_src($image_id, 'event-cover-arc');
        $image_id       = get_post_thumbnail_id();
        $event_location = get_post_meta($post->ID, "event_location", true);
        $event_venue    = get_post_meta($post->ID, "event_venue", true);
        $tstart         = get_post_meta($post->ID, 'event_tstart', true);
		$tstart_parsed 	= parseTime($tstart);
        $tend           = get_post_meta($post->ID, 'event_tend', true);
		$tend_parsed 	= parseTime($tend);
        $event_ticket   = get_post_meta($post->ID, "event_ticket", true);
		$event_text     = get_post_meta($post->ID, "ev_text", true);
        $event_out      = get_post_meta($post->ID, "event_out", true);
        $event_cancel   = get_post_meta($post->ID, "event_cancel", true);
        $event_zoom     = get_post_meta($post->ID, "event_zoom", true);
        $coordinated    = get_post_meta($post->ID, "event_coordinated", true);
        $club           = get_post_meta($post->ID, "event_venue", true);
        $event_allday   = get_post_meta($post->ID, "event_allday", true, true);

        $event_conductor= get_post_meta($post->ID, 'event_conductor', true);    
        $event_conductor_link= get_post_meta($post->ID, 'event_conductor_link', true);    
        $event_soloists  = get_post_meta($post->ID, 'event_soloists', true);    
        $event_soloists_links  = get_post_meta($post->ID, 'event_soloists_links', true);
        $event_no_tickets = get_post_meta($post->ID, "event_no_tickets", true);
        //$event_orchestra= _e(get_post_meta($post->ID, 'event_orchestra', true));  

$event_soloists_arr = explode("\n", $event_soloists);
if(count($event_soloists_arr) == 1)
    $event_soloists_arr = explode(",", $event_soloists);

$event_soloists_links_arr = explode("\n", $event_soloists_links);
if(count($event_soloists_links_arr) == 1)
    $event_soloists_links_arr = explode(",", $event_soloists_links);

$j = 0;
$solo_arr_size = count($event_soloists_arr);
$link_arr_size = count($event_soloists_links_arr);

        echo '
    <div class="event-cover">';
        if ($image_id) {
            echo '
      <img src="' . $cover[0] . '" alt="' . get_the_title() . '" />';
        } else {
            echo '
      <img src="' . get_template_directory_uri() . '/images/no-featured/event-single.png" alt="no image" />';
        }
        if ($data_event != null) {
            echo '        
      <div class="event-single-data">
        <div class="event-single-day">' . $pretty_date_d . '</div>
        <div class="event-single-month">' . ( pll_current_language() == 'en' ? $pretty_date_M2 : date_declension($pretty_date_M2)) . '</div>
        <div class="event-single-year">' . $pretty_date_yy . '</div>
      </div>';            
        }
        echo '</div><!-- end .event-cover -->';
        echo '
    <div class="event-text">
      <h2 class="event-title">' . get_the_title($post->ID) . '</h2>
        <ul class="event-meta">';
        if ($event_venue != null) {
            echo '<li><span style="vertical-align:top">';
            echo pll_e("miejsce");
            echo ':</span>';
            echo $event_venue;
            echo '</li>';
        }
		$is_previous_edition = (get_edition_number($post->ID) == get_current_edition_number(get_option('obecna-radio')) || is_front_page() || !has_term('','edycjanr')) ? false : true;
		$conductor = get_post($event_conductor);
		$event_previous_link;
		if($is_previous_edition) {
			$event_conductor_link = $GLOBALS['edition_parmalink'] . (pll_current_language() == 'en' ? 'artists' : 'artysci') . '/#' . $conductor->post_name;
		} else {
			$event_conductor_link = get_bloginfo('url') . '/' . (pll_current_language() == 'en' ? 'conductors' : 'dyrygenci') . '/#' . $conductor->post_name;
		}
		
        echo '<li><span>'; echo pll_e("godzina"); echo ':</span>' . $tstart_parsed . '</li>';
        echo $event_conductor != '2584' && $event_conductor != '2585' ? '<li><span style="vertical-align:top">' . pll__("dyrygent") . ':</span><span style="width:auto;color:#000"><a href="' . $event_conductor_link . '">' . $conductor->post_title . '</a></span></li>' : '';
        echo '<li class="solisci-list"><span style="vertical-align:top">'; echo pll_e("solisci"); echo  ':</span><span style="width:auto;color:#000">';

        global $post;
        $post = get_page( get_the_ID() );
        $childargs = array(
            'post_type' => 'solista-event',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'orderby' => 'meta_value_num',
            'meta_key' => 'wpcf-kolejnosc',
            'meta_query' => array(array('key' => '_wpcf_belongs_event_id', 'value' => get_the_ID()))
        );
        $query = new WP_Query($childargs);
        while ( $query->have_posts() ) : $query->the_post(); 
			$soloist = get_post(types_render_field('solista-wybierz'));
			if($is_previous_edition) :
			?>
				<div>
					<a <?php echo 'href="'. $GLOBALS['edition_parmalink'] . (pll_current_language() == 'en' ? 'artists' : 'artysci') . '/#' . $soloist->post_name.'"' ;?>>
						<?php echo $soloist->post_title; ?>
					</a>
				</div>
			<?php 
			else : 
			?>
				<div>
					<a <?php echo 'href="'.get_bloginfo('url') . '/' . (pll_current_language() == 'en' ? 'soloists' : 'solisci') . '/#' . $soloist->post_name.'"' ;?>>
						<?php echo $soloist->post_title; ?>
					</a>
				</div>
			<?php
			endif;

		endwhile;
		wp_reset_query();
		
		?>
		
			</span>
			</li>
			<li>
				<span style="margin-right: 0;"><?php pll_e("orkiestra"); ?>:</span>
				<a style="float:none" href="<?php _e(get_post_meta($post->ID, 'event_orchestra_link', true)); ?>"><?php _e(get_post_meta($post->ID, 'event_orchestra', true)); ?></a>
			</li>
		</li>

      
	<?php 
    if (get_edition_number($post->ID) == get_current_edition_number(get_option('obecna-radio')) ) :
	if (get_post_meta($post->ID, 'event_disable', true) == 'no') {
        if ($event_text) {
            if($event_no_tickets == 'on')
                echo '<div class="event-tickets3" style="display:inline-block"><span class="bilety_link" style="font-size:12px;margin-top:9px;margin-left:0px;width:auto">' . pll__('no_ticket') . '</span></div>';
            else
                echo '<div class="event-tickets3" style="display:inline-block"><a class="bilety_link" style="font-size:12px;margin-top:9px;margin-left:0px;" href="' . $event_ticket . '" target="_blank">' . pll__('buy_ticket') . '</a></div>';
            echo '<div class="blog-arc-more" style="display:inline-block"><a class="bilety_link" style="margin:9px 0 2px 10px;padding:5px 8px;" href="' . get_permalink(get_page_by_title(pll_current_language() == 'en' ? 'tickets' : 'bilety')) . '">' ;
         pll_e('ticket_info'); 
         echo   '</a></div>';
        } else {  
            if (get_post_meta($post->ID, 'event_out', true) == 'yes') {
                echo '
                        <div class="event-cancel-out"><p>Sold Out</p></div>';
            } elseif (get_post_meta($post->ID, 'event_cancel', true) == 'yes') {
                echo '
                        <div class="event-cancel-out"><p>Canceled</p></div>';
            } elseif (get_post_meta($post->ID, 'event_free', true) == 'yes') {
                echo '
                        <div class="event-cancel-out"><p>Free Entry</p></div>';
            } else {
                echo '
                        <div class="event-tickets"><a href="' . $event_ticket . '" target="_blank">';
         pll_e('buy_ticket_1'); 
         echo '</a></div>';
            }
        }
	}
    endif;

        echo '</li>
        </ul><!-- end ul.event-meta -->';
        echo the_content();
    echo '</div><!-- end .event-text -->';
        if (pll_current_language() == 'pl') :
            echo '<p style="text-align: justify;float:left;margin:20px">' . types_render_field('krotki-opis', array('output' => 'raw')) .'</p>';
        else :
            echo '<p style="text-align: justify;float:left;margin:20px">' . types_render_field('krotki-opis-en', array('output' => 'raw')) .'</p>';
        endif;
        if ($coordinated != null) {
            echo '
       <script type="text/javascript">
      jQuery(document).ready(function($){
      
        $("#event-map").gmap3({
          marker:{
            latLng: [' . $coordinated . '],
            options:{
              draggable:true
            },
            events:{
              dragend: function(marker){
                $(this).gmap3({
                  getaddress:{
                    latLng:marker.getPosition(),
                    callback:function(results){
                      var map = $(this).gmap3("get"),
                        infowindow = $(this).gmap3({get:"infowindow"}),
                        content = results && results[1] ? results && results[1].formatted_address : "no address";
                      if (infowindow){
                        infowindow.open(map, marker);
                        infowindow.setContent(content);
                      } else {
                        $(this).gmap3({
                          infowindow:{
                            anchor:marker, 
                            options:{content: content}
                          }
                        });
                      }
                    }
                  }
                });
              }
            }
          },
          map:{
            options:{
              zoom: ' . $event_zoom . '
            }
          }
        });
        
      });
    </script>
 
    <div id="event-map"></div>';
        }
        echo '       
  </div><!-- end .single-col -->';
    endwhile;
?>

</div><!-- end .fixed -->
</div><!-- end #content -->
	
<?php
get_footer();
?>