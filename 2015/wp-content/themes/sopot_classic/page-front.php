<?php
/*
Template Name: HOME PAGE
*/
?>

<?php
get_header();
?>

<?php
global $post;
$location      = str_replace(array(
    strtolower(get_bloginfo('url'))
), '', strtolower(get_permalink()));

$page_layout   = sidebar_layout();
$slide_nr      = of_get_option('nr_slide');
$slide_seconds = of_get_option('seconds_slide');
if (strlen($location) > 4) {
    echo '
<div class="title-head"><h1>' . get_the_title() . '</h1></div>';
    switch ($page_layout) {
        case "layout-sidebar-left":
            echo '<div class="fixed">';
            echo '
<div class="content-left">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .content-left -->';
            echo '
<div class="sidebar-left">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-left -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-sidebar-right":
            echo '<div class="fixed">';
            echo '
<div class="content-right">';
        
        echo 'aktualności';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .content-right -->';
            echo '
<div class="sidebar-right">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-right -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-full":
            echo '
<div class="single-page-col">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .single-page-col -->';
            break;
    }
} else {
    //echo 'xx'.get_current_edition_number(get_option('obecna-radio'));
    //if (of_get_option('active_slide', '1') == '1') {
        $wp_slider_query = new WP_Query(array(
			'posts_per_page' => -1,
            'post_type' => 'slide',
            'orderby' => 'DATE',
            'order' => 'DESC' ,
            'edycjanr' => get_current_edition_number(get_option('obecna-radio'))
        ));
        echo '
<div id="slide"> 
  <div class="cycle-slideshow" data-cycle-timeout="' . $slide_seconds . '" data-cycle-next="#next1" data-cycle-prev="#prev1" data-cycle-slides="div.slide">
    <a id="prev1" href="#"><div class="cycle-prev"></div></a>
    <a id="next1" href="#"><div class="cycle-next"></div></a>';
        if ($wp_slider_query->post_count) {
            while ($wp_slider_query->have_posts()):
                $wp_slider_query->the_post();
                $custom    = get_post_custom($post->ID);
                $image_id  = get_post_thumbnail_id();
                $cover     = wp_get_attachment_image_src($image_id, 'slider-full');
                $slide_des = $custom["slide_des"][0];
                $slide_url = $custom["slide_url"][0];
                $slide_title = get_the_title();
                echo '
    <div class="slide">';
                if ($slide_url != null) {
                    echo '
      <a href="' . $slide_url . '">';
                }
                echo '
        <img src="' . $cover[0] . '" alt="' . get_the_title() . '" />';
                if ($slide_url != null) {
                    echo '
      </a>';
                }
                if ($slide_url != null) {
                    echo '
      <a href="' . $slide_url . '">';
                }
                
                if ($slide_title != null) {
                echo '
        <div class="slide-title">' . $slide_title . '</div>';
                }
                
                if ($slide_url != null) {
                    echo '
      </a>';
                }
                if ($slide_url != null) {
                    echo '
      <a href="' . $slide_url . '">';
                }
                if ($slide_des != null) {
                    echo '
        <div class="slide-desc">' . $slide_des . '</div>';
                }
                if ($slide_url != null) {
                    echo '
      </a>';
                }
                echo '
    </div><!-- end .slide -->';
            endwhile;
            echo '
  </div><!-- end .cycle-slideshow -->
</div><!-- end #slide -->';
        }
    //}
    switch ($page_layout) {
        case "layout-sidebar-left":
            echo '
<div class="fixed">';
            echo '
<div class="content-home-left">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .content-home-left -->';
            echo '
<div class="sidebar-left">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-left -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-sidebar-right":
            echo '
<div class="fixed">';
            echo '
<div class="content-home-right">';
        
        echo '<div class="title-home"><h3>'; 
        pll_e('aktualnosci'); 
        echo '</h3></div>';
        
         ?>
        <?php 
        
        
        $custom_query = new WP_Query('post_type=post&edycjanr=' . get_current_edition_number(get_option('obecna-radio')) .'');
       
        echo '<div class="home-post">';
while($custom_query->have_posts()) : $custom_query->the_post(); 

        global $post;
        $image_id    = get_post_thumbnail_id();
        $cover_blog  = wp_get_attachment_image_src($image_id, 'blog-home');    
        $pretty_date_M2  = iconv("ISO-8859-2","UTF-8", get_the_time('j F Y'));


?>
        <div class="home-width fixed">
        <div class="blog-home">
            
      <?php if ($image_id) : 
         echo    '<div class="blog-home-cover">
          <a href="' . get_permalink() . '">
            <img src="' . $cover_blog[0] . '" alt="' . get_the_title() . '" />
          </a>
        </div><!-- end .blog-home-cover --> ';
       endif; 
        echo
        '<h2 class="event-arc-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>
        <div class="blog-home-info">
          
          <p class="blog-date">' . (pll_current_language() == 'en' ? get_the_time('F jS, Y') : date_declension($pretty_date_M2)) . '</p> 
          
        </div><!-- end .blog-home-info -->
        <p>' . the_excerpt_max(200) . '</p>
        <div class="blog-arc-more"><a href="' . get_permalink() . '" rel="bookmark">';
        pll_e('more'); 
        echo '</a></div>
      </div><!-- end .blog-home -->                   
    </div><!-- end .home-width fixed -->';
    endwhile;
    wp_reset_query();
       echo '</div><!-- end .home-post -->'; 
     ?>   
	<div class="title-home">
		<h3><?php pll_e('zdjecia'); ?></h3>
	</div>
	
	<div class="home-post fixed">
		<div class="col-home">
			<div class="home-width">
			<?php	
				$editions = get_terms('edycjanr', 'orderby=name&order=DESC&hide_empty'); 

				foreach($editions as $edition) :
					if($edition->slug != get_current_edition_number(get_option('obecna-radio'))) :
						$query1 = array(
							'post_type' => 'photo',
							'posts_per_page' => 1,
							'edycjanr' => $edition->slug,
							'orderby' => 'date',
							'order' => 'DESC'
						); 
						$wp_query_photo = new WP_Query($query1);
						
						$edition_pages = get_posts(array(
							'post_type' => 'page',
							'posts_per_page' => -1,
							'post_status' => 'publish',
							'edycjanr' => $edition->slug
						));
						
						$gallery_address;
						foreach($edition_pages as $edition_page) {
							if($edition_page->post_name == (pll_current_language() == 'en' ? 'gallery' : 'galeria')) {
								$gallery_address = get_permalink($edition_page->ID);
								break;
							}
						}

						while ($wp_query_photo->have_posts()):
							$wp_query_photo->the_post();
							global $post;
							$image_id    = get_post_thumbnail_id();
							$cover_photo = wp_get_attachment_image_src($image_id, 'photo-home');
							?>
							
							<div class="photo-home last-p">
								<div class="photo-home-cover bar-home-photo">
									<a href="<?php echo $gallery_address; ?>">
									<?php
										if ($image_id) {
											echo '<img src="' . $cover_photo[0] . '" alt="' . get_the_title() . '" />';
										} else {
											echo '<img src="' . get_template_directory_uri() . '/images/no-featured/photo-video-home.png" alt="no image" />';
										}
									?>
										<div class="media-home-title mosaic-overlay">             
											<?php echo pll_e($edition->name); ?>
										</div><!-- end .audio-title -->    
									</a>
								</div><!-- end .photo-home-cover -->          
							</div><!-- end .photo-home last-p -->
						<?php 
						endwhile;
						
						wp_reset_query();
					endif;
				endforeach;
			?>
			</div><!-- end .home-width -->
		</div><!-- end .col-home -->
	</div><!-- end .home-post fixed-->
        


	

        
        <?php
        
        
        
            echo '
</div><!-- end .content-home-right -->';
            echo '
<div class="sidebar-right">';
            wz_setSection('zone-sidebar');
            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
            echo '
</div><!-- end .sidebar-right -->';
            echo '
</div><!-- end .fixed -->';
            break;
        case "layout-full":
            echo '
<div class="single-page-col">';
            if (have_posts())
                while (have_posts()):
                    the_post();
                    echo the_content();
                endwhile;
            echo '
</div><!-- end .single-page-col -->';
            break;
    }
}
?>


<?php
get_footer();
?>