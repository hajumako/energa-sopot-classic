<?php

add_action( 'wp_enqueue_scripts', 'sc_scripts' );

function sc_scripts() {    
    wp_enqueue_script( 'google analytics', get_template_directory_uri() . '/js/ga.js', array(), '1.0.0', false );
    //wp_enqueue_script( 'mainjs', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', false );
    wp_enqueue_style( 'fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', array(), '1.0.0', false );
    wp_enqueue_script( 'fancybox', get_template_directory_uri() . '/js/jquery.fancybox.js', array(), '1.10.1', true );
}

function setLanguage() {
	if(pll_current_language() == 'en') {
		setlocale(LC_ALL, 'en_EN');
	} else {
		$arrLocales = array('pl_PL', 'pl','Polish_Poland.28592');
		setlocale(LC_ALL, $arrLocales);
	}
}

function debug($data) {
    /*if(is_array($data) || is_object($data))
	{
		echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
	} else {
		echo("<script>console.log('PHP: ".$data."');</script>");
	}*/
}

function parseTime($time) {
	$t_arr = explode(' ', $time);
	if(pll_current_language() == 'en') {
		if(strtolower($t_arr[1]) == 'pm' || strtolower($t_arr[1] == 'am')) {
			return $time;
		} else {
			$t_arr2 = split(':', $t_arr[0]);
			if($t_arr2[0] > 12) {
				$h = $t_arr2[0] - 12;    
				return $h . ':' . $t_arr2[1] . ' pm';
			} else {
				return $t_arr[0] + ' am';
			}
		}
	} else if(pll_current_language() == 'pl') {
		if(strtolower($t_arr[1] == 'am')) {
			return $t_arr[0];
		} else if(strtolower($t_arr[1]) == 'pm') {
			$t_arr2 = split(':', $t_arr[0]);
            $h = $t_arr2[0] + 12; 
            return $h . ':' . $t_arr2[1];
		} else {
			return $time;
		}
	}
}

/*** FUNCTIONS
 ****************************************************************/
include('includes/functions-comment.php');
include('includes/functions-setup.php');
include('includes/functions-menu.php');
include('includes/functions-layout.php');
include('includes/functions-sidebar.php');

/*** ADMIN POSTS
 ****************************************************************/
include('admin/audio.php');
include('admin/video.php');
include('admin/photo.php');
include('admin/event.php');
include('admin/slide.php');
include('admin/options.php');

/*** WIDGETS
 ****************************************************************/
include('includes/widgets/widget-twitter.php');
include('includes/widgets/widget-flickr.php');
include('includes/widgets/widget-blog.php');
include('includes/widgets/widget-events.php');
include('includes/widgets/widget-videos.php');
include('includes/widgets/widget-photos.php');
include('includes/widgets/widget-audio.php');
include('includes/widgets/widget-soundcloud.php');

/*** SHORTCODES
 ****************************************************************/
include('includes/shortcodes/shortcode-posts.php');
include('includes/shortcodes/shortcode.php');
include('includes/shortcodes/shortcode-soundcloud.php');

/*** EXCERPT
 ****************************************************************/
function custom_excerpt_length($length) {
    return 45;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

function new_excerpt_more($excerpt) {
    return str_replace('[...]', '...', $excerpt);
}
add_filter('wp_trim_excerpt', 'new_excerpt_more');

function the_excerpt_max_event($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '...';
	} else {
		echo $excerpt;
	}
}
function the_excerpt_max($charlength) {
	$items_src   = null;
	$excerpt = get_the_excerpt();
	$charlength++;
	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$items_src .= ' ' . mb_substr( $subex, 0, $excut ) . ' ';
			$items_src .= '...';
			return $items_src;
		} else {
			return $subex;
		}
	} else {
		return $excerpt;
	}
}

/*** PAGE NAVIGATION
 ****************************************************************/

function pagination($pages = '', $range = 4) {
    $showitems = ($range * 2) + 1;
    global $paged;
    if (empty($paged))
        $paged = 1;
    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }
    if (1 != $pages) {
        echo "<div class=\"pagination\">";
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link(1) . "'>&laquo; First</a>";
        if ($paged > 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo; Previous</a>";
        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? "<span class=\"current\">" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class=\"inactive\">" . $i . "</a>";
            }
        }
        if ($paged < $pages && $showitems < $pages)
            echo "<a href=\"" . get_pagenum_link($paged + 1) . "\">Next &rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($pages) . "'>Last &raquo;</a>";
        echo "</div>\n";
    }
}

/*** CATEGORY POST TYPES
 ****************************************************************/
function cat_post_types() {
    global $post;
    if (is_single() && !is_attachment()) {
        if (get_post_type() != 'post') {
            $post_type = get_post_type_object(get_post_type());
            $slug      = $post_type->rewrite;
            echo '' . $post_type->labels->singular_name . '';
        }
    }
}

add_filter('sidebars_widgets', 'disable_footer_widgets');

function disable_footer_widgets($sidebars_widgets) {
    if (is_single())
        $sidebars_widgets['audio_widget'] = false;
    return $sidebars_widgets;
}

/*** TAGCLOUD FONT SIZE
 ****************************************************************/
add_filter('widget_tag_cloud_args', 'wz_tag_cloud_filter', 90);

function wz_tag_cloud_filter($args = array()) {
	$args['smallest'] = 14;
	$args['largest'] = 14;
	$args['unit'] = 'px';
	return $args;
}

/*** ALLOW SVG UPLOAD
***************************************************************/
add_filter( 'upload_mimes', 'cc_mime_types' );

function cc_mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

add_action('after_setup_theme','remove_core_updates');

function remove_core_updates() {
	if(! current_user_can('update_core')){return;}
	add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
	add_filter('pre_option_update_core','__return_null');
	add_filter('pre_site_transient_update_core','__return_null');
}

/*** REGISTER TRANSLATIONS
 ****************************************************************/
pll_register_string('Edycja 10', 'Edycja 10');
pll_register_string('Edycja 9', 'Edycja 9');
pll_register_string('Edycja 8', 'Edycja 8');
pll_register_string('Edycja 7', 'Edycja 7');
pll_register_string('Edycja 6', 'Edycja 6');
pll_register_string('Edycja 5', 'Edycja 5');
pll_register_string('Edycja 4', 'Edycja 4');
pll_register_string('Edycja 3', 'Edycja 3');
pll_register_string('Edycja 2', 'Edycja 2');
pll_register_string('Edycja 1', 'Edycja 1');
 
pll_register_string('wspolpraca', 'wspolpraca');
pll_register_string('patroni-medialni', 'patroni-medialni');
pll_register_string('partnerzy', 'partnerzy');
pll_register_string('sponsorzy', 'sponsorzy');
pll_register_string('patroni-honorowi', 'patroni-honorowi');
pll_register_string('mecenas-festiwalu', 'mecenas-festiwalu');
pll_register_string('poprz_ed', 'poprz_ed');
pll_register_string('festiwal_ed', 'festiwal_ed');
pll_register_string('wydarzenie', 'wydarzenie');
pll_register_string('logo-miasto', 'logo-miasto');
pll_register_string('organizatorzy', 'organizatorzy');
pll_register_string('sponsor', 'sponsor');
pll_register_string('sponsor-tytularny', 'sponsor-tytularny');
pll_register_string('miejsce', 'miejsce');
pll_register_string('dyrygent', 'dyrygent');
pll_register_string('godzina', 'godzina');
pll_register_string('miejsce', 'miejsce');
pll_register_string('orkiestra', 'orkiestra');
pll_register_string('aktualnosci', 'aktualnosci');
pll_register_string('zdjecia', 'zdjecia');
pll_register_string('more', 'more');
pll_register_string('less', 'less');
pll_register_string('dyrygenci', 'dyrygenci');
pll_register_string('solisci', 'solisci');
pll_register_string('solisci2', 'solisci2');
pll_register_string('prowadzacy', 'prowadzacy');
pll_register_string('edycja', 'edycja');
pll_register_string('bilety', 'bilety');
pll_register_string('buy_tickets', 'buy_tickets');
pll_register_string('buy_ticket', 'buy_ticket');
pll_register_string('no_ticket', 'no_ticket');
pll_register_string('buy_ticket_1', 'buy_ticket_1');
pll_register_string('ticket_info', 'ticket_info');



/************* Konrad START ***************************************/

add_action( 'admin_enqueue_scripts', 'disableSlugEdit' );

function disableSlugEdit($hook) {
    if ( 'post.php' != $hook ) {
        return;
    }

    wp_enqueue_script( 'disable_slug_edit', get_template_directory_uri() . '/admin/post/js/disable_slug_edit.js' );
}

add_action( 'admin_menu', 'addEditionMenu' );

function addEditionMenu(){
    add_submenu_page('edit.php?post_type=edycja', 'Edycje - ustawienia', 'Aktualna edycja', 'manage_options', 'edycje-current.php', 'edycje_function' );
}

function edycje_function() {?>
    <div class="wrap">
        <h2>Aktualna edycja</h2>
        
        <?php 
            if($_POST) {                
                update_option( "obecna-radio", $_POST['obecna-radio'] );
            } 
        ?>
        
        <form method="post">
            <?php wp_nonce_field('update-options') ?>           
            <p><strong>Wybierz aktualną edycję festiwalu:</strong><br/>               
            <table>                 
                <?php $posts = get_posts(array(
                    'post_type' => 'edycja',
                    'post_status' => 'publish,draft'
                ));
    
                foreach($posts as $post) :
                    $post_term_id = get_post_meta($post->ID, 'post_term_id', true);
                    $selected = ( get_option('obecna-radio') == $post_term_id) ? 'checked="checked"' : ''; 
                ?>
                    <input type="hidden" value="<?php echo $post->ID ?>" name="pages[]" >
                    <tr>
                        <td><?php echo $post->post_title;?></td>
                        <td><input <?php echo $selected ?> type="radio" name="obecna-radio" value="<?php echo $post_term_id; ?>"></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            </p>
            <p>
                <?php $nr_obecna= get_option('obecna-radio'); ?>
                OBECNA EDYCJA: <?php echo $nr_obecna;?>
            </p>
            <input type="submit" name="Submit" value="Zapisz" >
            <input type="hidden" name="action" value="update" >
        </form>
    </div>
    <?php 
}

add_action('save_post', 'save_post_mod', 100000);

function save_post_mod( $post_id ){
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
    foreach((array)get_post_type($post_id) as $post_type) {
        if($post_type == 'edycja') {
            $edition_page_meta_ids = array (
                'pl' => 'pl_post_edition_page_id',
                'en' => 'en_post_edition_page_id'
            );
            
            $term_name = get_the_title($post_id);
            $post_term_id = get_post_meta($post_id, 'post_term_id', true);
            if($post_term_id == "") {
                wp_insert_term( $term_name, 'edycjanr' );
            } else {
                wp_update_term( $post_term_id, 'edycjanr', array( 'name' => $term_name ) );
            }
            $new_post_term_id = get_term_by( 'name', $term_name, 'edycjanr' )->term_id;
            update_post_meta( $post_id, 'post_term_id', $new_post_term_id );   
            wp_set_object_terms( $post_id, array($term_name), 'edycjanr' );
            
            remove_action('save_post', 'save_post_mod', 100000);          
            
            $post_status = $_POST['wpcf']['published'] == '1' ? 'publish' : 'draft';
            
            
            $edition_page = array(
                'post_content'   => '',
                'post_status'    => $post_status,
                'post_type'      => 'page',
                'tax_input'      => array( 'edycjanr' => array($new_post_term_id) ),
                'page_template'  => 'templates/edycja.php'
            );
            
            $ed_no = preg_replace("/[^0-9]/", "", $term_name);
            
            $edition_page['ID'] = get_post_meta( $post_id, $edition_page_meta_ids['pl'], true );              
            $edition_page['post_name'] = get_localized_edition_name($term_name, 'pl', 'slug');             
            $edition_page['post_title'] = get_localized_edition_name($term_name, 'pl', 'title');           
            $pl_edition_page_id = wp_insert_post( $edition_page, true );
            pll_set_post_language( $pl_edition_page_id, 'pl' );
            update_post_meta( $post_id, $edition_page_meta_ids['pl'], $pl_edition_page_id ); 
                       
            $edition_page['ID'] = get_post_meta($post_id, $edition_page_meta_ids['en'], true );
            $edition_page['post_name'] = get_localized_edition_name($term_name, 'en', 'slug');            
            $edition_page['post_title'] = get_localized_edition_name($term_name, 'en', 'title'); 
            $en_edition_page_id = wp_insert_post( $edition_page, true );
            pll_set_post_language( $en_edition_page_id, 'en' );      
            update_post_meta( $post_id, $edition_page_meta_ids['en'], $en_edition_page_id );  
            
            pll_save_post_translations( array( 'pl' => $pl_edition_page_id, 'en' => $en_edition_page_id ) );
            
            createEditionChildPages(
                $post_id, 
                array( 
                    'pl' => $pl_edition_page_id, 
                    'en' => $en_edition_page_id
                ),
                $post_status, 
                $new_post_term_id
            );
            
            add_action('save_post', 'save_post_mod', 100000);
        } else if ($post_type == 'post') {
			update_post_meta( $post_id, 'pic_align', $_POST['pic_align'] );
		}
    }
}

function createEditionChildPages($post_id, $parent_ids, $post_status, $post_term_id) {
    $post_names = array(
        'pl' => array(
            'aktualnosci',
            'artysci',
            'galeria',
            'program'
        ),
        'en' => array(
            'news',
            'artists',
            'gallery',
            'programme'
        )
    );
    $post_titles = array(
        'pl' => array(
            'Aktualności',
            'Artyści',
            'Galeria',
            'Program'
        ),
        'en' => array(
            'News',
            'Artists',
            'Gallery',
            'Programme'
        )
    );   
    $post_meta_ids = array(
        'pl' => array(
            'pl_post_aktualnosci_page_id',
            'pl_post_artysci_page_id',
            'pl_post_galeria_page_id',
            'pl_post_program_page_id'
        ),
        'en' => array(
            'en_post_aktualnosci_page_id',
            'en_post_artysci_page_id',
            'en_post_galeria_page_id',
            'en_post_program_page_id'
        )
    );    
    $post_templates = array(
        'templates/aktualnosci_old.php',
        'templates/artysci_old.php',
        'templates/galeria_old.php',
        'program_old.php'
    );
    
    $i = 0;
    foreach($post_templates as $post_template) {
        $subpage = array(
            'post_content'   => '',
            'post_status'    => $post_status,
            'post_type'      => 'page',
            'tax_input'      => array( 'edycjanr' => array($post_term_id) ),
            'page_template'  => $post_template,
            'menu_order'     => $i
        );
              
        $subpage['ID'] = get_post_meta($post_id, $post_meta_ids['pl'][$i], true);         
        $subpage['post_name'] = $post_names['pl'][$i];         
        $subpage['post_title'] = $post_titles['pl'][$i];         
        $subpage['post_parent'] = $parent_ids['pl'];         
        $pl_subpage_id = wp_insert_post( $subpage, true );     
        pll_set_post_language($pl_subpage_id, 'pl');
     
        update_post_meta($post_id, $post_meta_ids['pl'][$i], $pl_subpage_id); 
        
        $subpage['ID'] = get_post_meta($post_id, $post_meta_ids['en'][$i], true);         
        $subpage['post_name'] = $post_names['en'][$i];         
        $subpage['post_title'] = $post_titles['en'][$i];         
        $subpage['post_parent'] = $parent_ids['en'];       
        $en_subpage_id = wp_insert_post( $subpage, true );     
        pll_set_post_language($en_subpage_id, 'en');
     
        update_post_meta($post_id, $post_meta_ids['en'][$i], $en_subpage_id);  

        pll_save_post_translations(array('pl' => $pl_subpage_id, 'en' => $en_subpage_id));
        
        $i++;
    }
}

add_action( 'before_delete_post', 'before_delete_post_mod' );

function before_delete_post_mod( $post_id ){
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
    foreach((array)get_post_type($post_id) as $post_type) {
        if($post_type == 'edycja') {
            $edition_page_meta_ids = array (
                'pl' => 'pl_post_edition_page_id',
                'en' => 'en_post_edition_page_id'
            );
            $post_meta_ids = array(
                'pl' => array(
                    'pl_post_aktualnosci_page_id',
                    'pl_post_artysci_page_id',
                    'pl_post_galeria_page_id',
                    'pl_post_program_page_id'
                ),
                'en' => array(
                    'en_post_aktualnosci_page_id',
                    'en_post_artysci_page_id',
                    'en_post_galeria_page_id',
                    'en_post_program_page_id'
                )
            );
            $langs = array('pl', 'en');
            foreach($langs as $lang) {
                wp_delete_post(get_post_meta($post_id, $edition_page_meta_ids[$lang], true), false);
                $id = 0;
                foreach($post_meta_ids[$lang] as $post_meta_id) {
                    wp_delete_post(get_post_meta($post_id, $post_meta_id[$i++], true), false);   
                }
            }
            wp_delete_term( get_post_meta($post_id, 'post_term_id', true), 'edycjanr');
        }
    }
}

function get_localized_number($name) {
	$edition_no = preg_replace("/[^0-9]/", "", $name);
    if(pll_current_language() == 'en') {
        $suffix;
        $tens = $edition_no % 100;
        if($tens == 11 || $tens == 12 || $tens == 13)
            $suffix = '<span style="text-transform:lowercase">th</span>';
        else {
            $singles = $tens % 10;
            if($singles == 1)
                $suffix = '<span style="text-transform:lowercase">st</span>';
            else if($singles == 2)
                $suffix = '<span style="text-transform:lowercase">nd</span>';
            else if($singles == 3)
                $suffix = '<span style="text-transform:lowercase">rd</span>';
            else
                $suffix = '<span style="text-transform:lowercase">th</span>';
        }  
        return $edition_no . $suffix;
    } else {
        return $edition_no;
	}
}

function get_localized_edition_number($name) {
    $edition_no = preg_replace("/[^0-9]/", "", $name);
    if(pll_current_language() == 'en') {
        $suffix;
        $tens = $edition_no % 100;
        if($tens == 11 || $tens == 12 || $tens == 13)
            $suffix = 'th';
        else {
            $singles = $tens % 10;
            if($singles == 1)
                $suffix = 'st';
            else if($singles == 2)
                $suffix = 'nd';
            else if($singles == 3)
                $suffix = 'rd';
            else
                $suffix = 'th';
        }  
        return $edition_no . $suffix . ' <span>edition</span>';
    } else {
		$table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
		$roman_no = ''; 
		while($edition_no > 0) 
		{ 
			foreach($table as $rom=>$arb) 
			{ 
				if($edition_no >= $arb) 
				{ 
					$edition_no -= $arb; 
					$roman_no .= $rom; 
					break; 
				} 
			} 
		} 
        return $roman_no . ' <span>edycja</span>';
	}
}

function get_localized_edition_name($name, $lang, $type) {
    $edition_no = preg_replace("/[^0-9]/", "", $name);
    if($lang == 'en') {
        $suffix;
        $tens = $edition_no % 100;
        if($tens == 11 || $tens == 12 || $tens == 13)
            $suffix = 'th';
        else {
            $singles = $tens % 10;
            if($singles == 1)
                $suffix = 'st';
            else if($singles == 2)
                $suffix = 'nd';
            else if($singles == 3)
                $suffix = 'rd';
            else
                $suffix = 'th';
        }  
        return $edition_no . $suffix . ($type == 'slug' ? '-edition' : ' Edition');
    } else
        return $edition_no . ($type == 'slug' ? '-edycja' : ' Edycja');
}

function get_edition_number($post_id) {
    $terms = get_the_terms( $post_id, 'edycjanr' );
    if ( $terms && ! is_wp_error( $terms ) ) { 
        return $terms[0]->slug;
    }
}

function get_current_edition_number($current_term_id) {
    $term = get_term( $current_term_id, 'edycjanr' );
    if ( $term && ! is_wp_error( $term ) ) { 
        return $term->slug;
    }
}

/************* Konrad END ***************************************/

// Creating the widget "Logotypes"
class wpb_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
			'wpb_widget', 

			// Widget name will appear in UI
			__('Logotypy Footer', 'wpb_widget_domain'), 

			// Widget description
			array( 'description' => __( 'Wyświetla logotypy w footerze', 'wpb_widget_domain' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output
		//GET THE NUMBER OF EDITION
//			$terms = get_the_terms( $post->ID, 'edycjanr' );
//
//			if ( $terms && ! is_wp_error( $terms ) ) : 
//
//				$edycjanr = array();
//
//				foreach ( $terms as $term ) {
//					$edycjanr[] = $term->name;
//				}
//
//				$nazwaedycji = join( ", ", $edycjanr );
//
//			 endif; 
//		$edition_number = preg_replace("/[^0-9]/","",$nazwaedycji);
//		$nr_obecna = get_option('obecna-radio');

		if (get_edition_number($post->ID) == get_current_edition_number(get_option('obecna-radio')) || is_front_page() || !has_term('','edycjanr')) : ?>
			
			<h5><?php pll_e('organizatorzy'); ?></h5>
			<div class="pat">
			<?php global $post;
			$post = get_page_by_title('Footer-Organizatorzy');
			$child_posts = types_child_posts('logotyp');
			foreach ($child_posts as $child_post): ?>
			   <div>
					<a <?php echo 'href="'.$child_post->fields['link'].'"' ;?> target="_blank">
						<img <?php echo 'src="'.$child_post->fields['logotypy'].'"' ;?> /> 
					</a>
				</div>
			<?php endforeach;?>
			</div>
			<div class="pat">
				<div style="max-width:200px">
					<h5><?php pll_e('logo-miasto'); ?></h5>
					<?php 
					$post = get_page_by_title('Footer-logo1');
					$child_posts = types_child_posts('logotyp');
					foreach ($child_posts as $child_post): ?>
					<a <?php echo 'href="'.$child_post->fields['link'].'"' ;?> target="_blank">
						<img <?php echo 'src="'.$child_post->fields['logotypy'].'"' ;?> /> 
					</a>
			<?php endforeach;?>
				</div>
				<div style="max-width:200px">
					<h5 style="margin-bottom: 27px;"><?php pll_e('sponsor-tytularny'); ?></h5>
					<?php 
					$post = get_page_by_title('Footer-logo2');
					$child_posts = types_child_posts('logotyp');
					foreach ($child_posts as $child_post): ?>
					<a <?php echo 'href="'.$child_post->fields['link'].'"' ;?> target="_blank">
						<img <?php echo 'src="'.$child_post->fields['logotypy'].'"' ;?> /> 
					</a>
			<?php endforeach;?>
				</div>
			</div>
			</div>

	<?php else : ?>

			 <h5><?php pll_e('organizatorzy'); ?></h5>
			<div class="pat">
				<div>
					<a href="http://www.pfksopot.pl/" target="_blank">
						<img src="http://sopotclassic.pl/wp-content/uploads/2014/07/znak_PFK_a.jpg" alt="Polska Filharmonia Kameralna Sopot" />
					</a>
				</div>
				<div>
					<a href="http://www.bart.sopot.pl/" target="_blank">
						<img src="http://sopotclassic.pl/wp-content/uploads/2014/07/bart4.jpg" alt="BART" />
					</a>
				</div>
				<div>
					<a href="http://operalesna.sopot.pl/" target="_blank">
						<img src="http://sopotclassic.pl/wp-content/uploads/2014/07/org3.jpg" alt="Opera Leśna w Sopocie" />
					</a>
				</div>
			</div>

			<div class="pat">
				<div>
					<a href="http://urzad.pomorskie.eu/pl" target="_blank">
						<img alt="logo" src="http://sopotclassic.pl/wp-content/uploads/2014/07/marszałek.jpg" />
					</a>
				</div>
				<div>
					<a href="http://www.sopot.pl/eGmina/pl" target="_blank">
						<img alt="logo" src="http://sopotclassic.pl/wp-content/uploads/2014/07/plansze_film_loga-1.jpg" />
					</a>
				</div>
			</div>

			<div class="pat">
				<div style="max-width:200px">
					<h5><?php pll_e('logo-miasto'); ?></h5>
					<a href="http://www.sopot.pl" target="_blank">
						<img alt="logo" src="http://sopotclassic.pl/wp-content/uploads/2014/07/sopot.jpg" />
					</a>
				</div>
				<!--<div style="max-width:200px">
					<h5 style="margin-bottom: 27px;"><?php pll_e('sponsor'); ?></h5>
					<a href="http://www.energa.pl" target="_blank">
						<img alt="logo" src="http://sopotclassic.pl/wp-content/uploads/2014/07/energa.jpg" />
					</a>
				</div>-->
			</div>
			</div>
	<?php
		endif;
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = __( 'New title', 'wpb_widget_domain' );
		}
		// Widget admin form
		?>
			<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
			return $instance;
	}
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

add_action( 'admin_menu', 'adjust_the_wp_menu', 999 );
function adjust_the_wp_menu() {
	remove_menu_page('edit-comments.php');
	remove_menu_page('edit.php?post_type=audio');

	remove_submenu_page('edit.php?post_type=edycja', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=edycja');
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=edycjanr');
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');	
	remove_submenu_page('edit.php?post_type=video', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=video');
	remove_submenu_page('edit.php?post_type=video', 'edit-tags.php?taxonomy=on-main-page&amp;post_type=video');
	remove_submenu_page('edit.php?post_type=video', 'edit-tags.php?taxonomy=videos&amp;post_type=video');
	remove_submenu_page('edit.php?post_type=photo', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=photo');
	remove_submenu_page('edit.php?post_type=photo', 'edit-tags.php?taxonomy=photos&amp;post_type=photo');
	remove_submenu_page('edit.php?post_type=event', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=event');
	remove_submenu_page('edit.php?post_type=event', 'edit-tags.php?taxonomy=events&amp;post_type=event');		
	remove_submenu_page('edit.php?post_type=page', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=page');
	remove_submenu_page('edit.php?post_type=dyrygent', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=dyrygent');
	remove_submenu_page('edit.php?post_type=solista', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=solista');
	remove_submenu_page('edit.php?post_type=prowadzacy-1', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=prowadzacy-1');
	remove_submenu_page('edit.php?post_type=slide', 'edit-tags.php?taxonomy=edycjanr&amp;post_type=slide');
}

add_action( 'do_meta_boxes', 'hide_meta_boxes');

//HIDE ELEMENTS
function hide_meta_boxes() {
    global $post;
	
	hide_div_by_id('wpcf-marketing');
	
	remove_meta_box( 'categorydiv', 'post' , 'side' );
	remove_meta_box( 'categorydiv', 'page' , 'side' );
	remove_meta_box( 'categorydiv', 'edycja' , 'side' );
	
	remove_meta_box( 'tagsdiv-post_tag', 'post' , 'side' );
	remove_meta_box( 'tagsdiv-post_tag', 'page' , 'side' );
	remove_meta_box( 'tagsdiv-post_tag', 'photo' , 'side' );
	remove_meta_box( 'tagsdiv-post_tag', 'video' , 'side' );
	remove_meta_box( 'tagsdiv-post_tag', 'event' , 'side' );
	remove_meta_box( 'tagsdiv-post_tag', 'edycja' , 'side' );
	
	remove_meta_box( 'edycjanrdiv', 'edycja' , 'side' );
	remove_meta_box( 'edycjanrdiv', 'page' , 'side' );
	//remove_meta_box( 'postimagediv', 'page' , 'side' );
	remove_meta_box( 'photosdiv', 'photo' , 'side' );  
	remove_meta_box( 'videosdiv', 'video' , 'side' );  
	remove_meta_box( 'eventsdiv', 'event' , 'side' );  
	
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	if(get_post_type($post_id) == 'edycja') { 
		hide_div_by_id('screen-meta-links');  
	}
	hide_div_by_id('edit-slug-box .edit-slug-buttons');
	hide_div_by_id('edit-slug-box .view-post-btn');
	hide_div_by_id('edit-slug-box a');

	$template_file = get_post_meta($post_id, '_wp_page_template', true);
	
    if($template_file != 'templates/logotypy.php' && $template_file != 'templates/wspolpraca.php' && get_post_type($post_id) != 'event')
		remove_meta_box( 'wpcf-post-relationship', 'page' , 'normal' );  
	if($template_file != 'templates/logotypy.php')
		hide_div_by_id('types-child-table-page-logotyp');
	if($template_file != 'templates/wspolpraca.php')
		hide_div_by_id('types-child-table-page-logotyp-wspolpraca');
	if(get_post_type($post_id) != 'event')
		hide_div_by_id('types-child-table-event-solista-event');
	
	if(get_post_type($post_id) == 'event') {
		if (get_post_language($post_id) == 'EN')
			remove_meta_box( 'wpcf-group-opis-dla-wydarzenia', 'event' , 'normal' );  
		else
			remove_meta_box( 'wpcf-group-opis-dla-wydarzenia-en', 'event' , 'normal' );  
	}
}

function hide_div_by_id($id) {
	echo '<style>#'.$id.'{display: none !important;}</style>'; 
}

//MENU CHILD

function wpb_list_child_pages($depth = 0) { 
	global $post;
	$parent_post = get_post($post->post_parent);
	$ancestor_id = $parent_post->post_parent ? $parent_post->post_parent : $parent_post->ID;
	$descendants = get_pages(array('child_of' => $ancestor_id));
	$incl = "";

	foreach ($descendants as $descendant) {
	   if (($descendant->post_parent == $ancestor_id) ||
		   ($descendant->post_parent == $post->post_parent))
	   {
		  $incl .= $descendant->ID . ",";
	   }
	   
	}

	echo '<ul>'.wp_list_pages(array("child_of" => $ancestor_id, "include" => $incl, "link_before" => "", "title_li" => "", "sort_column" => "menu_order", "depth" => $depth)).'</ul>';
}

function get_short_event_title($title) {
	$title_arr = explode(' ', $title);
	//var_dump($title_arr);
	$short_title;
	foreach($title_arr as $title_word) {
		if(strlen($short_title . ' ' . $title_word) > 34) {
			$short_title .= ' ...';
			break;
		} else {
			$short_title .= ' ' . $title_word;
			}
	}
	return $short_title;
}

add_action( 'admin_menu', 'change_post_menu_label' );
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Aktualności';
    $submenu['edit.php'][5][0] = 'Wszystkie aktualności';
    $submenu['edit.php'][10][0] = 'Dodaj aktualność';
    echo '';
}

add_action( 'init', 'change_post_object_label' );
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Aktualności';
	$labels->singular_name = 'Aktualność';
	$labels->add_new = 'Dodaj nową';
	$labels->add_new_item = 'Dodaj nową aktualność';
	$labels->edit_item = 'Edit aktualność';
	$labels->new_item = 'Aktualność';
	$labels->view_item = 'Zobacz aktualność';
	$labels->search_items = 'Szukaj aktualności';
}

function get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'featured_preview');
        return $post_thumbnail_img[0];
    }
}

function get_post_language($post_id, $slug = 'name') {
    $a_language = wp_get_post_terms($post_id, 'language');
    return $a_language[0]->$slug;
}

?>