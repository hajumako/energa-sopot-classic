<?php
get_header();
?>


<div id="content">

<div class="title-head"><h1>
    <?php 
        $post_type = get_post_type(get_the_ID());
    if ( $post_type ==  'post' ) :
        pll_e('aktualnosci');
    elseif ( $post_type ==  'dyrygent' || 'solista' || 'prowadzacy-1' ) :
        echo $post_type;
    else:
        pll_e('aktualnosci');
    endif;?>
</h1></div>

<?php
$page_layout = of_get_option('blog_images');
switch ($page_layout) {
    case "left-blog-sidebar":
        echo '
<div class="sidebar-left">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-left -->';
        break;
    case "right-blog-sidebar":
        echo '
<div class="sidebar-right">';
        wz_setSection('zone-sidebar');
        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-page'));
        echo '
</div><!-- end .sidebar-right -->';
        break;
}
?>

<div class="fixed">
  <div class="single-col">					
<?php
if (have_posts())
    while (have_posts()):
        the_post();
        $img_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        $image_id = get_post_thumbnail_id($post->ID);
		$align = get_post_meta( $post->ID, 'pic_align', true );
		$cover;
		if($align != '')
			$cover = wp_get_attachment_image_src($image_id, 'blog-preview-' . $align);
		else
			$cover = wp_get_attachment_image_src($image_id, 'blog-preview');
for($k = 0; $k < count(cover); $k++) {
}
        $pretty_date_M2  = iconv("ISO-8859-2","UTF-8", get_the_time('j F Y'));
        if ($image_id) {
            echo '
    <div class="blog-arc-cover">     
        <a href="'. $img_url .'" class="fancybox" rel="group"><img src="' . $cover[0] . '" alt="' . get_the_title() . '" /></a>
    </div><!-- end .blog-arc-cover -->';
        }
        echo '  
    <h2 class="blog-arc-heading">' . get_the_title($post->ID) . '</h2>
      <div class="blog-arc-info">';
        
        // <p class="blog-date">' . (pll_current_language() == 'en' ? get_the_time('F jS, Y') : date_declension($pretty_date_M2)) . '</p> 
        
      echo'</div><!-- end .blog-arc-info -->';
        echo "<p>" . the_content() . "</p>";
    endwhile;
?>			
				
<?php
//comments_template('', true);
?>

    </div><!-- end .single-col -->			
</div><!-- end .fixed -->		 
</div><!-- end #content -->
	
<?php
get_footer();
?>