(function ($) {

  $(document)
    .ready(function () {
	
        $('.accordion-more').on('click', function() {
            $(this).nextAll('.blog-home').css('height','auto');
            $(this).fadeOut();
            $(this).nextAll('.accordion-less').fadeIn();
        });
      
       $('.accordion-less').on('click', function() {
            $(this).nextAll('.blog-home').css('height','190px');
            $(this).fadeOut();
            $(this).prevAll('.accordion-more').fadeIn();
        });
	
	});

})(window.jQuery);