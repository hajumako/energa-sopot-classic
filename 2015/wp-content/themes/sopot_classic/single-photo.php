<?php
get_header();
?>



<div id="content">

<div class="title-head"><h1><?php
the_title();
?>
</h1></div>
<?php
$galleries_items_nr = array();
$galleries_items_title = array();

$cnt_images = generate_thumbnail_list(get_the_ID());

array_push($galleries_items_nr, $cnt_images);
array_push($galleries_items_title, get_the_title());
?>  

</div><!-- end #content -->


<script type="text/javascript">
    var galleries_items_nr = <?php echo json_encode($galleries_items_nr); ?>;
    var galleries_items_title = <?php echo json_encode($galleries_items_title); ?>;
	
	function get_gallery_title(position) {
		cnt = 0;
		sum = galleries_items_nr[0];
		while(position > sum) {
			sum += galleries_items_nr[++cnt];
		}
		return galleries_items_title[cnt];
	}
</script>

	
<?php
get_footer();
?>
