</div><!-- end #wrap -->

<!-- Footer -->
<div id="footer">
  <div class="footer-row fixed">
			
<?php
wz_setSection('zone-footer');
?>
    <div class="footer-col-left">
<?php
wz_setZone(220);
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-left'));
?>
    <?php 
	if (pll_current_language() == 'pl'):
		echo 
		'<h5>O Festiwalu</h5>
<img alt="logo" src="http://sopotclassic.pl/wp-content/uploads/2015/06/sopotclassic_LOGO_zpodpisem2.jpg" style="width: 100%;"/>
<p>Międzynarodowy Festiwal Muzyczny Sopot Classic powstał w 2011 roku z inicjatywy Dyrektora Artystycznego Polskiej Filharmonii Kameralnej Sopot Wojciecha Rajskiego oraz władz Miasta Sopotu. 
Każdego roku Sopot Classic gości znakomitych artystów z Polski i zagranicy, których interpretacje najdoskonalszych dzieł muzyki od Baroku po XXI wiek wypełniają koncertowe programy.
Festiwal wspiera i pobudza twórczość młodych kompozytorów. Imprezą towarzyszącą jest Konkurs kompozytorski im. Krzysztofa Pendereckiego, który odbywa się w cyklu dwuletnim i jest integralną częścią Festiwalu.</p>'
		;
	else: 
		echo 
		'<h5>About Festival</h5>
<img alt="logo" src="http://sopotclassic.pl/wp-content/uploads/2015/06/sopotclassic_LOGO_zpodpisem2.jpg" style="width: 100%;"/>
<p>The International Music Festival Sopot Classic was founded in 2011 by artistic director of the Polish Chamber Philharmonic Orchestra Sopot, Wojciech Rajski and the City of Sopot.
Each year the Sopot Classic hosts outstanding Polish and foreign artists, whose interpretations of the most splendid musical pieces, from Baroque to XXI century, fill the concert schedules.
The festival supports and stimulates the creativity of young composers. The biennial Krzysztof Penderecki Composers Competition is an associated event and an integral part of the Festival.</p>';
	endif;
	?>
    </div><!-- end .footer-col -->
				
    <div class="footer-col-loga">
<?php
wz_setZone(460);
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-center-left'));
?>
    
    </div><!-- end .footer-col -->
				
    <div class="footer-col-right">
<?php
wz_setZone(460);
if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-center-right'));
?>
    <?php 
	if (pll_current_language() == 'pl'):
		echo 
		'<h5>Kontakt</h5>
		<p><span class="contact">Biuro Festiwalowe SOPOT CLASSIC</span><br />
ul. Moniuszki 12<br />
81-729 Sopot</p>
<p>tel. <span><a href="mailto:+48 58 555 84 21">+48 58 555 84 21</a></span><br />
e-mail: <span><a href="mailto:sopotclassic@sopot.pl">sopotclassic@sopot.pl</a></span></p>'
		;
	else: 
		echo 
		'<h5>Contact</h5>
		<p><span class="contact">SOPOT CLASSIC Festival Office</span><br />
12 Moniuszki Street<br />
81-729 Sopot</p>
<p>phone: <span><a href="mailto:+48 58 555 84 21">+48 58 555 84 21</a></span><br />
e-mail: <span><a href="mailto:sopotclassic@sopot.pl">sopotclassic@sopot.pl</a></span></p>';
		endif;
	?>
    </div><!-- end .footer-col -->
				
    
  </div><!-- end .footer-row fixed -->			
</div><!-- end #footer -->

<div class="footer-bottom"> 
  <div class="footer-row">
    <div class="footer-bottom-copyright">
<?php
if (get_option("bc_copyright") != '') {
    echo stripslashes(get_option("bc_copyright"));
} else {
?>
&copy;
<?php
    $the_year = date("Y");
    echo $the_year;
?>
 Energa Sopot Classic

<?php
}
?>
    </div><!-- end .footer-bottom-copyright -->
    
    <div class="facebook">
    <a href="https://www.facebook.com/SopotClassic" target="_blank">
    	<img src="<?php echo get_template_directory_uri(); ?>/images/facebook.jpg" alt="facebook" />
    </a> 
    </div><!-- end .footer-bottom-social -->
    
    <div class="realizacja">
    <a href="http://quellio.com/" target="_blank" title="Quellio - We make your apps"><img src="<?php echo get_template_directory_uri(); ?>/images/quellio.png" alt="Quellio" /></a>
    </div>
    
    
  </div><!-- end .footer-row -->
</div><!-- end .footer-bottom -->


<?php  echo ' '. get_template_part( 'prettyPhotojs' ) . ''; ?>


<?php wp_footer(); ?>

</body>
</html>