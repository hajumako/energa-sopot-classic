<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<!-- ### BEGIN HEAD ####  -->
<head>

<!-- Meta -->
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<!-- Title -->
<title><?php 
	$prefix = false;
		
		 if (function_exists('is_tag') && is_tag()) {
			single_tag_title('Tag Archive for &quot;'); 
			echo '&quot; - '; 
			
			$prefix = true;
		 } elseif (is_archive()) {
			
			wp_title(''); echo ' '.__('Archive').' - '; 
			$prefix = true;
			
		 } elseif (is_search()) {
			
			echo __('Search for', 'clubber').' &quot;'.wp_specialchars($s).'&quot; - '; 
			$prefix = true;
			
		 } elseif (!(is_404()) && (is_single()) || (is_page())) {
				wp_title(''); 
				echo '  ';
		 } elseif (is_404()) {
			echo __('Not Found', 'clubber').' - ';
		 }
		 
		 if (is_home()) {
			bloginfo('name'); echo ' - '; bloginfo('description');
		 } else {
		  bloginfo('name');
		 }
		 
		 if ($paged > 1) {
			echo ' - page '. $paged; 
		 }
	?></title>

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicons/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/favicons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/favicons/mstile-144x144.png">
<meta name="msapplication-config" content="/favicons/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<!-- Stylesheets -->

<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css_options.php" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/quelio.css" type="text/css" media="screen" />

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" type="text/css" media="screen" />


<!-- Wordpress functions -->	
<?php wp_head(); ?>


<?php if(of_get_option('analytics_code')!="") {?>
<!-- Google analytics -->
<script type="text/javascript">
<?php echo of_get_option('analytics_code'); ?>
</script>
<?php } ?>
    
<script type="text/javascript">
    jQuery(document).ready(function($){
        var galeria_nazwa = "<?php echo $galeria_nazwa ?>";
    });
</script>
     

    
<?php    
    setLanguage();
?>
    
<!-- STYLE DEPENDING ON EDITION NUMBER   -->

<?php 

$edition_page_meta_ids = array (
	'pl' => 'pl_post_edition_page_id',
	'en' => 'en_post_edition_page_id'
);

$editions;

if (!has_term('','edycjanr')) {
	$editions = get_posts(array(
        'post_type' => 'edycja',
        'tax_query' => array(
            array(
                'taxonomy' => 'edycjanr',
                'field' => 'term_id',
                'terms' => get_option('obecna-radio')
            )
        )
    ));
	$GLOBALS['previous_edition'] = false;
	$GLOBALS['previous_edition2'] = 'n';
} else {           
	$terms = get_the_terms( $post->ID, 'edycjanr' );
    $editions = get_posts(array(
        'post_type' => 'edycja',
        'tax_query' => array(
            array(
                'taxonomy' => 'edycjanr',
                'field' => 'term_id',
                'terms' => $terms[0]->term_id
            )
        )
    ));
	$GLOBALS['previous_edition'] = true;
	$GLOBALS['previous_edition2'] = 'y ' . $terms[0]->name;
}
        
global $post;
$orig_post = $post;
$post = $editions[0];
$hex = str_split(trim(types_render_field('color6', array('output' => 'raw')), '#'), 2);
$transp_bg_color = 'rgba(' . hexdec($hex[0]) . ',' . hexdec($hex[1]) . ',' . hexdec($hex[2]) . ', .3)';
$wyglad = array(
    'bg_file' => types_render_field('bg_file', array('output' => 'raw')),
	'festival_logo' => types_render_field('festival-logo', array('output' => 'raw')),
    'color1' => types_render_field('color1', array('output' => 'raw')),
    'color2' => types_render_field('color2', array('output' => 'raw')),
    'color3' => types_render_field('color3', array('output' => 'raw')),
    'color4' => types_render_field('color4', array('output' => 'raw')),
    'color5' => types_render_field('color5', array('output' => 'raw')),
    'color6' => $transp_bg_color
);  
$year = types_render_field('year', array('output' => 'raw'));        

$GLOBALS['edition_parmalink'] = get_permalink(get_post_meta( $post->ID, $edition_page_meta_ids[pll_current_language()], true ));

$post = $orig_post;

?> 
    
<style type="text/css">   
    /* STYLE WITH PROPERTIES from admin if is NOT in tax 'edycjanr'*/
    body {
        background: url("<?php echo $wyglad['bg_file'] ?>") no-repeat top center !important;
    }
	#wrap {
		  background: <?php echo $wyglad['color6'] ?>
	}
    .title-home h3, .title-head h1, .title-head h3, .sidebar-right h3, .title-home.artysci h3 {
        color: <?php echo $wyglad['color5'] ?> !important;
    }
    .event-w-day, .event-arc-day, .event-single-day, .slide-title span {
        color: <?php echo $wyglad['color4'] ?> !important;
    }
    #footer {
        border-top: 10px solid <?php echo $wyglad['color3'] ?> !important;
    }
    a.edition-button {
        background: <?php echo $wyglad['color3'] ?> !important;
    }
	a.edition-button:hover {
        background: <?php echo $wyglad['color1'] ?> !important;
    }
	a.edition-button.current {
		background: #ffffff !important;
		border-color: <?php echo $wyglad['color3'] ?> !important;
		color: <?php echo $wyglad['color3'] ?> !important;
	}
	a.edition-button.current:hover {
		color: <?php echo $wyglad['color1'] ?> !important;
		border-color: <?php echo $wyglad['color1'] ?> !important;
	}
	a.edition-button.current span {
		color: <?php echo $wyglad['color3'] ?> !important;
	}
	a.edition-button.current:hover span {
		color: <?php echo $wyglad['color1'] ?> !important;
	}
    #clubbmenu > ul > li:hover > a, #clubbmenu > ul > li.active > a, #clubbmenu > ul > li > a:active, #clubbmenu > ul ul li a:hover {
        background: <?php echo $wyglad['color2'] ?> !important;
    }
    a.bilety_link, span.bilety_link {
        background-color: <?php echo $wyglad['color1'] ?> !important;
        border: <?php echo $wyglad['color1'] ?> 1px solid !important;
    }
	span.bilety_link:hover {
		color: #fff;
	}
    a.bilety_link:hover {
        background-color: white !important;
        border: <?php echo $wyglad['color1'] ?> 1px solid !important;
        color: <?php echo $wyglad['color1'] ?> !important;
    }
    .blog-arc-more a.bilety_link {
        background-color: white !important;
        border: 1px solid #aaaaaa !important;
    }
    .blog-arc-more a.bilety_link:hover {
        background-color: #aaaaaa !important;
        color: white !important;
    }    
    .editions-title {
        background: <?php echo $wyglad['color6'] ?>
    }
	
	.lang-item-pl a, .lang-item-en a {
		color: <?php echo $wyglad['color4'] ?> !important;
	}

	#clubbmenu > ul > li.lang-item-pl, 
	#clubbmenu > ul > li.lang-item-en {
			display: block;
	}	
	
	@media (max-width:800px) {	
		#clubbmenu > ul {
			text-align: center;
		}
		
		#clubbmenu > ul > li.lang-item-pl, 
		#clubbmenu > ul > li.lang-item-en {
			display: inline-block;
			width:auto !important;
			float: none;
		}	
	}
    
    @media (min-width:800px) and (max-width:1200px) {
        .editions {
            background: <?php echo $wyglad['color3'] ?> !important;
        }
    }
</style>
      
<!-- end of STYLE DEPENDING ON EDITION NUMBER   -->

	<?php
	$menu_ids = array ('', 1426, 1427, 1428, 1429, 2243, 2244, 2245);

	$post_id_orig = $post->ID;
	foreach($menu_ids as $i => $menu_id) {
		$post->ID = pll_current_language() == 'en' ? 1418 : 1414;
		$child_posts = types_child_posts('logotyp-wspolpraca');
		//$post_id_orig = $post->ID;

		$logos = 0;
		foreach ($child_posts as $child_post):
			$post->ID = $child_post->ID;

			if (types_render_field("logo-rodzaj", array("output" => "raw")) == $i) {
				$logos++;
			}
		endforeach;


		if ($logos == 0) {
			echo '<style type="text/css">#menu-item-' . $menu_id . '{ display:none !important;}</style>';
		}
	}
	$post->ID = $post_id_orig;
	?>

</head>
    
    
<!-- Begin Body -->
<body  <?php body_class(); ?>>

<!-- Header -->
<div id="header"> 			
  <div class="header-row fixed">		
    <div id="logo">					
<?php 	
	echo '<a href="'. get_bloginfo('url') .'"><img src="'.$wyglad['festival_logo'].'" alt="logo" /></a>';    
    echo '<div class="logo-info">';
    global $post;

    $term_nr = get_edition_number($post->ID);
    $term = get_term_by('slug', $term_nr, 'edycjanr');    

    $edition_page_id = get_post_meta( $post->ID, $edition_page_meta_ids[pll_current_language()], true );                 
    
    if (!(get_edition_number($post->ID) == get_current_edition_number(get_option('obecna-radio')) || is_front_page() || !has_term('','edycjanr'))) {
		echo '<div class="logo-ed-nr">' . get_localized_edition_number($term->name) . '</div>';
		echo '<div class="logo-year">' . $year . '</div>';
    }
    echo '</div>'; /*end .logo-info */
?>
    </div><!-- end #logo -->
	
    <div id="main">
	  <div id="btn-menu">MENU</div>
      <div class="main-navigation">
				<?php 

                if (get_edition_number($post->ID) == get_current_edition_number(get_option('obecna-radio')) || is_front_page() || !has_term('','edycjanr')) {				

					$menu = wp_nav_menu(array(
					   'theme_location' => 'top-menu', 
					   'container_id' => 'clubbmenu', 
					   'walker' => new CSS_Menu_Maker_Walker(),
					   'echo' => false
					)); 
					$menu_arr = explode(pll_current_language() == 'en' ? 'Programme</a>' : 'Program</a>', $menu);

					echo $menu_arr[0];					
					echo pll_current_language() == 'en' ? 'Programme</a>' : 'Program</a>';					
					echo '<ul><li class="menu-item menu-item-type-custom menu-item-object-custom">';
					
					//$term  = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
					//$events_nr = of_get_option('nr_events'); 
					$query = array(
						'post_type' => 'event',
						'taxonomy' => 'events',
                        'posts_per_page' => -1,
						'edycjanr' => get_current_edition_number(get_option('obecna-radio'))
					);
					$wp_query = new WP_Query($query);
					while ($wp_query->have_posts()) {
						$wp_query->the_post();
						global $post;
						$results = $wp_query->post_count;
						$data_event     = get_post_meta($post->ID, 'event_date_interval', true);
						$time           = strtotime($data_event);
						$pretty_date_yy = date('Y', $time);
						$pretty_date_d  = date('d', $time);
						$pretty_date_M2  = iconv("ISO-8859-2","UTF-8", ucfirst(strftime('%B', $time)));
						require('includes/language.php');
						$permalink = get_permalink();

						echo '<a href="' . $permalink . '">' . (pll_current_language() == 'en' ? $pretty_date_M2 . ' ' . get_localized_number($pretty_date_d) : $pretty_date_d . ' ' . date_declension($pretty_date_M2)) .'  '. $pretty_date_yy . '</a>';
					}				
					echo '</li></ul>';					
					echo $menu_arr[1];					
                } else {

					echo '<div id="clubbmenu" class="menu-menu-container">
                        <ul id="menu-menu" class="menu">';
                    
                    $parent = (is_page_template( 'edycja.php' )) ? '$post->ID' :  'wp_get_post_parent_id($post->ID)';
                    
                    $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $parent,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order',
                        'edycjanr' 		 => get_edition_number($post->ID)
                    ); 
                    $childrens = new WP_Query( $args );

                    if ( $childrens->have_posts() ) {
						while ( $childrens->have_posts() ) { 
							$childrens->the_post();
					  
							echo '<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="' . get_permalink() . '">'; 
							
							$child_name = get_the_title();
							echo $child_name . '</a>';
							
							if ( $child_name == 'Program' || $child_name == 'Programme' ) {
								echo '<ul><li class="menu-item menu-item-type-custom menu-item-object-custom">';
								
								$term  = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
								$events_nr = of_get_option('nr_events'); 
								$query = array(
									'post_type' => 'event',
									'posts_per_page' => $events_nr,
									'paged' => $paged,
									'taxonomy' => 'events',
									'term' => $term->slug,
									'edycjanr' => get_edition_number($post->ID)
								);
								$wp_query = new WP_Query($query);
								 while ($wp_query->have_posts()): $wp_query->the_post();
									global $post;
									$results = $wp_query->post_count;
									$data_event     = get_post_meta($post->ID, 'event_date_interval', true);
									$time           = strtotime($data_event);
									$pretty_date_yy = date('Y', $time);
									$pretty_date_d  = date('d', $time);
									$pretty_date_M2  = iconv("ISO-8859-2","UTF-8", ucfirst(strftime('%B', $time)));
									require('includes/language.php');
									$permalink = get_permalink();

									echo '<a href="' . $permalink . '">' . $pretty_date_d .'  '. (pll_current_language() == 'en' ? $pretty_date_M2 : date_declension($pretty_date_M2)) .'  '. $pretty_date_yy . '</a>';

								endwhile;
								
								echo '</li></ul>';
							}

							echo '</li>';
						}
						pll_the_languages();
					}
                    
					echo'</ul></div>';
				} 		
				wp_reset_query(); ?> 
                <div id="clubbmenu" class="menu-menu-container editions-mobile">
                            <div class="menu-editions-old" style=" border-top: 1px solid white;">                              
							<?php 
								$is_current_edition_page = (get_edition_number($post->ID) == get_current_edition_number(get_option('obecna-radio')) || is_front_page() || !has_term('','edycjanr'));
								if($is_current_edition_page)
									echo '<span>' . pll__('poprz_ed') . '</span>';
								else
									echo '<span>' . pll__('festiwal_ed') . '</span>'; 
							


								$editions = get_posts(array(
									'post_type' => 'edycja',
									'post_status' => 'publish',
									'orderby' => 'title',
									'order' => 'DESC',
								));
								
								global $post;
								$post_orig = $post;

								foreach($editions as $edition){
									$post = $edition;

									if(types_render_field('published') && (get_the_terms( $post->ID, 'edycjanr' )[0]->term_id != get_option('obecna-radio') || !$is_current_edition_page)) {
										$edition_page_id = get_post_meta( $post->ID, $edition_page_meta_ids[pll_current_language()], true );        
										if(get_the_terms( $post->ID, 'edycjanr' )[0]->term_id == get_option('obecna-radio')) {
											echo '<a href="' . get_bloginfo('url') . '" class="edition-button current">' . get_localized_edition_number($post->post_title) . '</a>';
										} else {
											echo '<a href="' . get_permalink($edition_page_id) . '" class="edition-button">' . get_localized_edition_number($post->post_title) . '</a>';
										}
									} 
								}


                if (pll_current_language() == 'en') :
                    ?> 
                    <style type="text/css">
                        a.edition-button{
                            padding: 13px 0 0 5px;
                            width: 42px;
                        }
                    </style>
                <?php
                endif;

                $post = $post_orig;
                ?>
                                
                                
                                
                                
                            </div>
                    </div>
      </div><!-- end .main-navigation -->			
    </div><!-- end #main -->
  </div><!-- end .header-row fixed -->           
</div><!-- end #header -->

<!-- Wrap -->
<div id="wrap">
    
    <div class="editions" style="position: fixed; left:0; top:40%; z-index: 300;">
        <?php 
		if($is_current_edition_page)
			echo '<div class="editions-title">' . pll__('poprz_ed') . '</div>';
		else
			echo '<div style="width:170px" class="editions-title">' . pll__('festiwal_ed') . '</div>';


        $editions = get_posts(array(
            'post_type' => 'edycja',
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'DESC',
			'posts_per_page' => -1
        ));

        $edition_page_meta_ids = array (
            'pl' => 'pl_post_edition_page_id',
            'en' => 'en_post_edition_page_id'
        );

        global $post;
        $post_orig = $post;
		
        foreach($editions as $edition){
            $post = $edition;
           
            if(types_render_field('published') && ( get_the_terms( $post->ID, 'edycjanr' )[0]->term_id != get_option('obecna-radio') || !$is_current_edition_page)) {
                $edition_page_id = get_post_meta( $post->ID, $edition_page_meta_ids[pll_current_language()], true );        
				if(get_the_terms( $post->ID, 'edycjanr' )[0]->term_id == get_option('obecna-radio')) {
					echo '<a href="' . get_bloginfo('url') . '" class="edition-button current">' . get_localized_edition_number($post->post_title) . '</a>';
				} else {
					echo '<a href="' . get_permalink($edition_page_id) . '" class="edition-button">' . get_localized_edition_number($post->post_title) . '</a>';
				}
            }            
        }

        if (pll_current_language() == 'en') :
            ?> 
            <style type="text/css">
                a.edition-button{
                    padding: 13px 0 0 5px;
                    width: 42px;
                }
            </style>
        <?php
        endif;

        $post = $post_orig;
        ?>
         <a class="facebook_btn" href="https://www.facebook.com/SopotClassic" target="_blank">
            <img src="<?php bloginfo('template_url') ?>/images/facebook.png">
        </a>   
    </div>
    
     