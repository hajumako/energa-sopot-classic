<?php
add_action('init', 'video_type');
add_action('init', 'video_taxonomies', 0);
add_action('admin_menu', 'videos_youtube_or_vimeo');
add_action('save_post', 'videos_save');
add_filter('manage_edit-video_columns', 'add_new_video_columns');
add_action('manage_video_posts_custom_column', 'manage_video_columns', 10, 2);
function video_type() {
    $imagepath = get_stylesheet_directory_uri() . '/images/posticon/';
    $labels    = array(
        'name' => __('Filmy', 'clubber'),
        'singular_name' => __('Film', 'clubber'),
        'add_new' => __('Dodaj nowy', 'clubber'),
        'add_new_item' => __('Dodaj nowy film', 'clubber'),
        'edit' => __('Edytuj', 'clubber'),
        'edit_item' => __('Edytuj film', 'clubber'),
        'new_item' => __('Nowy film', 'clubber'),
        'view' => __('Zobacz film', 'clubber'),
        'view_item' => __('Zobacz film', 'clubber'),
        'search_items' => __('Szukaj filmu', 'clubber'),
        'not_found' => __('Nic nie znaleziono', 'clubber'),
        'not_found_in_trash' => __('Nic nie znaleziono w koszu', 'clubber'),
        'parent_item_colon' => ''
    );
    $args      = array(
        'labels' => $labels,
        'description' => 'This is the holding location for all Videos',
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'rewrite' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => $imagepath . '/video.png',
        'supports' => array(
            'title',
            'thumbnail'
        )
    );
    register_post_type('video', $args);
}
function video_taxonomies() {
    register_taxonomy('videos', 'video', array(
        'hierarchical' => true,
        'slug' => 'videos',
        'label' => __('Category', 'clubber'),
        'query_var' => true,
        'rewrite' => true
    ));
}
function videos_youtube_or_vimeo() {
    add_meta_box('video_link', __('YouTube or Vimeo - Video', 'clubber'), 'video_link_meta_source', 'video', 'normal', 'high');
}
function videos_save($post_ID = 0) {
    $post_ID = (int) $post_ID;  
    $post_type = get_post_type( $post_ID );
    $post_status = get_post_status( $post_ID );    
    if ( "video" == $post_type && "auto-draft" != $post_status ) {
    update_post_meta($post_ID, "video_link", $_POST["video_link"]);
    }
}
function video_link_meta_source() {
    global $post;
    $video = get_post_meta($post->ID, 'video_link', true);
    echo '
<div style="padding-top:0px;">
<p>Podaj link do YouTube lub Vimeo: <input style="width:80%" name="video_link" value="' . $video . '" /></p>
</div>';
}
function add_new_video_columns() {
    $new_columns['cb']     = '<input type="checkbox" />';
    $new_columns['title']  = __('Tytuł', 'clubber');
    $new_columns['author'] = __('Autor', 'clubber');
    $new_columns['edycjanr'] = 'Edycja';
    $new_columns['zdjecie']  = 'Zdjęcie';
    $new_columns['date']   = __('Data', 'clubber');
    return $new_columns;
}
function manage_video_columns($column_name, $id) {
    global $wpdb;
    switch ($column_name) {
        case 'id':
            echo $id;
            break;
		case 'edycjanr':
			$terms = get_the_terms( $id, 'edycjanr' );
			foreach($terms as $term)
				echo $term->name . '<br>';
			break;
		case 'zdjecie':
			$post_featured_image = get_featured_image($id);
			if ($post_featured_image)
				echo '<img style="max-width:150px;max-height:150px" src="' . $post_featured_image . '" />';
			break;
        case 'images':
            // Get number of images in gallery
            $num_images = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->posts WHERE post_parent = {$id};"));
            echo $num_images;
            break;
        default:
            break;
    } // end switch
}
?>